package controllers;

import java.util.Vector;

import models.Comment;
import database.CommentDB;

public class CommentController {
	public static boolean newComment(String text, int announcementID, int userID) {
		return CommentDB.insertComment(announcementID, userID, ControllerFunctions.checkString(text));
	}
	
	public static Vector<Comment> getComment(int announcementID) {
		return CommentDB.fetchComment(announcementID);
	}
	
	public static boolean newCommentEvent(String text, int announcementID, int userID) {
		return CommentDB.insertComment(announcementID, userID, ControllerFunctions.checkString(text));
	}
	
	public static Vector<Comment> getCommentEvent(int announcementID) {
		return CommentDB.fetchComment(announcementID);
	}
}
