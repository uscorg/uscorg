package controllers;

import java.sql.Timestamp;
import java.sql.Date;
import java.util.Vector;

import database.AnnouncementDB;
import database.EventDB;
import database.UserDB;
import models.Announcement;
import models.CurrentObjects;
import models.Event;

public class EventController {
	public static String newEvent(int orgID, int year, int month, int day, String text, String name, int time) {
		if (text.length() > 160) {
			return "Event descriptions must be less than 160 characters!";
		}
		 if(EventDB.insertEvent(orgID, year, month, day, ControllerFunctions.checkString(text), ControllerFunctions.checkString(name), time)) {
			 return "Success!";
		 }
		 else {
			 return "Error";
		 }
		//CurrentObjects.setEvent(EventDB.fetchEvent(orgID).);
	
	}
	public static Event makeTempEvent() {
		
		Timestamp time = new Timestamp(0);

		return new Event(1, "event", "name", new Timestamp(0), new Timestamp(0), 2222, 4, 15);
		//EventDB.insertEvent

	}
	public static Vector <Event> getEvent(int orgID) {
		return EventDB.fetchEvent(orgID);
	}
}
