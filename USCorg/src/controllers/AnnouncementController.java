package controllers;

import java.util.Vector;

import models.Announcement;
import database.AnnouncementDB;
import database.EventDB;

public class AnnouncementController {
	public static String newAnnouncement(int orgID, int userID, String title, String text) {
		if (text.length() > 550) {
			return "Announcement text must be less than 550 characters!";
		}
		 if(AnnouncementDB.insertAnnouncement(orgID, userID, ControllerFunctions.checkString(title), ControllerFunctions.checkString(text))) {
			 return "Success!";
		 }
		 else {
			 return "Error";
		 }
	}
	
	public static Vector <Announcement> getAnnouncement(int orgID) {
		return AnnouncementDB.fetchAnnouncement(orgID);
	}
}
