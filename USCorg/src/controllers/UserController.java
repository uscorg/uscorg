package controllers;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import models.Announcement;
import models.Comment;
import models.CurrentObjects;
import models.Org;
import models.User;
import utils.PasswordHash;
import views.CurrentOrgPanel;
import views.MainFrame;
import views.NotificationPanel;
import database.UserDB;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class UserController {

	//returns true if login is successful, 
		//also sets currentUser to the logged in user if successful
		//else returns false
	public static boolean login(String username, String password){
		//TODO change the password verification once implemented hashing
		
		CurrentObjects.setUser(UserDB.fetchUserwithUsername(ControllerFunctions.checkString(username)));

		if (CurrentObjects.getUser() == null) {
			//user does not exist
			return false;
		} else {
			//user exists, must do a password check
			boolean passwordMatches = false;
			try {
				password = ControllerFunctions.checkString(password);
				passwordMatches = PasswordHash.validatePassword(password.toCharArray(), CurrentObjects.getUser().getPassword());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			}
			if (!passwordMatches) {
				//password does not match
				CurrentObjects.setUser(makeGuestUser());
				return false;
			} else {
				//password matches & user exists
				//login successful!
				NotificationPanel.changeLabels();
				CurrentObjects.getUser().setLoggedIn(true);
				
				return true;
			}
		}
	}
	
	//make currentUser into a guest user with dummy variables, 
	//called on startup/ logout 
	public static User makeGuestUser(){
		return new User(0, "00", "Tommy", "Trojan", "Guest", "pw", 00, "trojan", false);
	}
	
	//PBKDF2 password hashing (utilizes SHA1)
	public String hashPassword (String password){
		String hash = "";
		try {
			hash = PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return hash;
	}
	
	public static String registerCheck(String firstName, String lastName, String email, String uscID, String password, String confirmPassword, String major, int yearOf, String phoneNum){
		/*
		 * the registration function: tied to the registerButton in GUI
		 * returns true if the registration was successful
		 * returns false if one of the text fields was entered incorrectly
		 */
				
		if (!firstName.matches("[A-Za-z]+") || !lastName.matches("[A-Za-z]+")) {
			//first name and last name must consist of letters only
			return "Improper name (names must be one word and contain letters ONLY)";
		}
		if (!email.matches("[\\.A-Za-z0-9_]+@usc.edu")) {
			//email must be of the email format + @usc.edu is required
			return "Improper email (all emails must be @usc.edu)";
		}
		if (!uscID.matches("[0-9]{10}")) {
			//USCID must be exactly 10 digits long
			return "Improper USCID (must be 10 digits long)";
		}
		if (password.length() < 8) {
			//password can be any characters, but must have at least 8 characters
			return "Password too short (must be at least 8 characters long)";
		}
		if (!password.equals(confirmPassword)){
			//password != confirm password
			return "Password does not match the confirmation password";
		}

		String username = email.substring(0, email.length()-8); //converting email to username
		if(!UserDB.usernameIsValid(username)) {
			//username already exists
			return "This email is already registered to a user";
		}
		
		if(!UserDB.idIsValid(uscID)) {
			//ID already exists
			return "This USCID is already registered to a user";
		}

		Random random = new Random();
		int a = random.nextInt(999998 - 100001 + 1) + 100001;
		CurrentObjects.setVerification(a);
		sendText(phoneNum, a);
		
		return "SUCCESS";
	}
	public static void registerUser(String firstName, String lastName, String username, String uscID, String password, String major, int yearOf) {
		//hashing the password to send it to the database
		String passwordHash = "";
		try {
			passwordHash = PasswordHash.createHash(password);
		} catch (NoSuchAlgorithmException e) {e.printStackTrace();}
		catch (InvalidKeySpecException e) {e.printStackTrace();}
		
		UserDB.insertUser(uscID, firstName, lastName, passwordHash, major, yearOf, username);
		CurrentObjects.setUser(UserDB.fetchUserwithUsername(username));
		CurrentObjects.setAlmostRegisteredUser(makeGuestUser());
		CurrentObjects.getUser().setLoggedIn(true);
	}

	public static User newUser(String firstName, String lastName, String email, String uscID, String password, String major, int yearOf) {
		String username = email.substring(0, email.length()-8); //converting email to username
		return new User(0, uscID, firstName, lastName, password, major, yearOf, username, false);
	}
	
	public static String changeUserInfo(String studentID, String fname, String lname, String pw, String confirmPW, String major, int yearOf, String uscName) {
		
		if (fname.equals(null)) {
    		fname = models.CurrentObjects.getUser().getFirstName();
    	}
		else if (!fname.matches("[A-Za-z]+")) {
			return "Improper name (names must be one word and contain letters ONLY)";
		}
		
		if (lname.equals(null)) {
    		lname = models.CurrentObjects.getUser().getLastName();
    	}
		else if (!lname.matches("[A-Za-z]+")) {
			return "Improper name (names must be one word and contain letters ONLY)";
		}
		
		if (uscName.equals(null)) {
    		uscName = models.CurrentObjects.getUser().getUscName();
    	}
		else if (!uscName.matches("[\\.A-Za-z0-9_]+@usc.edu")) {
			return "Improper email (all emails must be @usc.edu)";
		}
		
		if (studentID.equals(null)) {
    		studentID = models.CurrentObjects.getUser().getStudentID();
    	}
		else if (!studentID.matches("[0-9]{10}")) {
			return "Improper USCID (must be 10 digits long)";
		}
		
		if (!pw.equals(confirmPW)){
			return "Password does not match the confirmation password";
		}
		if (pw.equals(null)) {
    		pw = models.CurrentObjects.getUser().getPassword();
    	}
		else if (pw.length() < 8) {
			return "Password too short (must be at least 8 characters long)";
		}
		
		if (major.equals(null)) {
    		major = models.CurrentObjects.getUser().getMajor();
    	}
		
		String username = uscName.substring(0, uscName.length()-8); //converting email to username
		if(!UserDB.usernameIsValid(username) && !username.equals(models.CurrentObjects.getUser().getUscName())) {
			//username already exists
			return "This email is already registered to a user";
		}
		
		if(!UserDB.idIsValid(studentID) && !studentID.equals(models.CurrentObjects.getUser().getStudentID())) {
			//ID already exists
			return "This USCID is already registered to a user";
		}
		
		//hashing the password to send it to the database
		String passwordHash = "";
		try {
			passwordHash = PasswordHash.createHash(pw);
		} catch (NoSuchAlgorithmException e) {e.printStackTrace();}
		catch (InvalidKeySpecException e) {e.printStackTrace();}

		UserDB.updateUser(studentID, fname, lname, passwordHash, ControllerFunctions.checkString(major), yearOf, username);
		CurrentObjects.setUser(UserDB.fetchUserwithUsername(username));
		
		return "SUCCESS";

	}
	
	public static boolean checkVerification(String verification) {
		int verificationInt = Integer.parseInt(verification);
		if (verificationInt == CurrentObjects.getVerification()){
			return true;
		}
		return false;
	}
	
	public static User getUser(int userID) {
		//gets a user given a user database ID
		return UserDB.fetchUserwithUsername(UserDB.fetchUsername(userID));
	}
	
	public static ArrayList<String> getUserOrganizationsNames(User user) {
		/* 
		 * this function returns an ArrayList<String> with the names
		 * of all organizations that the user is in.
		 * This will be used in UI for the sidebar listing current user's
		 * orgs, or for looking at other people's profiles.
		 * Returns an empty ArrayList<String> if the user has no organizations,
		 * otherwise just returns the list with the org names.
		 */
		if (UserDB.fetchUserOrganizations(user.getUserID()).isEmpty()) {
			//user has no organizations assigned
			return new ArrayList<String>();
		} else {
			//fetching the names of the organizations
			ArrayList<String> orgsList = new ArrayList<String>();
			Vector<Org> userOrgs = UserDB.fetchUserOrganizations(user.getUserID());
			for (Org entry : userOrgs) {
				orgsList.add(entry.getName());
			}
			return orgsList;
		}
	}
	
	public static boolean sendText(String number, int verification){
		TwilioRestClient client = new TwilioRestClient("AC641669bc0d51b167a70ba1b25d89f957", "fe2e7e3c1b69aefa675393bd3497076a");
		number = "+1" + number;
	    // Build a filter for the MessageList
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("Body", "Welcome to USC.Org!  Your verification code is " + verification));
	    params.add(new BasicNameValuePair("To", number));
	    params.add(new BasicNameValuePair("From", "+18723958876"));
	 
	    MessageFactory messageFactory = client.getAccount().getMessageFactory();
	    Message message;
		try {
			message = messageFactory.create(params);
		} catch (TwilioRestException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			return false;
		}
	    return true;
	}
	
	public static boolean addPermission(int userID, int orgID, String permission) {
		/*
		 * permission MUST be either ADMIN or MEMBER
		 */
		return UserDB.insertPermission(userID, orgID, permission);
	}
}
