package controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import utils.Pair;
import models.Announcement;
import models.Comment;
import models.CurrentObjects;
import models.Org;
import database.OrgDB;

public class OrgController {
	public static String newOrganization(String orgName, String d, String mt, String c, String membersNumber, String dues, String a) {
		if (orgName.isEmpty() || d.isEmpty() || mt.isEmpty() || c.isEmpty() || membersNumber.isEmpty() || dues.isEmpty() || a.isEmpty()) {
			//empty strings present
			return "Error creating organization! \nPlease try again!";
		}
		if (!OrgDB.orgNameIsValid(orgName)) {
			return "Organization already exists!";
		}
		if (d.length() > 100) {
			return "Organization descriptions must be less than 100 characters!";
		}
		//check audience
		if (a.equals("Both")) {
			a = "B";
		} else if (a.equals("Undergrad")) {
			a = "U";
		} else if (a.equals("Grad")) {
			a = "G";
		} else {
			//audience field is incorrect
			return "Error creating organization! \nPlease try again!";
		}
		int mn = Integer.parseInt(membersNumber);
		int du = Integer.parseInt(dues);
		if (OrgDB.insertOrg(orgName, d, mt, c, mn, du, a)) {
			return "Success!";
		}
		else {
			return "Error creating organization! \nPlease try again!";
		}
	}
	public static boolean updateOrganization(String orgName, String des, String mt, String contact, String memberNo, String dues, String audience, int orgID) {
		if (orgName.isEmpty() || des.isEmpty() || mt.isEmpty() || contact.isEmpty() || memberNo.isEmpty() || dues.isEmpty() || audience.isEmpty()) {
			return false;
		}
		//check audience
		if (audience.equals("Both")) {
			audience = "B";
		} else if (audience.equals("Undergrad")) {
			audience = "U";
		} else if (audience.equals("Grad")) {
			audience = "G";
		} else {
			//audience field is incorrect
			return false;
		}
		int memberNumber = Integer.parseInt(memberNo);
		int duesInt = Integer.parseInt(dues);
		
		return OrgDB.updateOrg(orgName, des, mt, contact, memberNumber, duesInt, audience, orgID);
	}

	public static Org makeTempOrg() {
		//String n, String d, String mt, String c, int mn, int du, String a, Map<int, String>users, Vector<Announcement> announcements, Vector<Comment> comments
		return new Org("temp", "ooo", "MWF", "hi@usc.edu", 10, 15, "UG", 1);
	}

	public static Vector< Org > searchOrgs(String keyword) {
		/*
		 * Calls the DB function, adding the % characters for SQL functionality.
		 * This finds orgs with the keyword in any part of the name.
		 */
		return OrgDB.searchOrgs("%" + ControllerFunctions.checkString(keyword) + "%");
	}
	public static Org getOrgWithID(int orgID) {
		//returns Org given its unique database ID
		return OrgDB.fetchOrgwithOrgname(OrgDB.fetchOrgName(orgID));
	}
	public static Org getOrgWithName(String name) {
		return OrgDB.fetchOrgwithOrgname(name);
	}
}
