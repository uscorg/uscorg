package controllers;

public class ControllerFunctions {
	public static String checkString(String s) {
		/*
		 * Used throughout the back end functions to check
		 * that the strings being passed to the database do
		 * not contain any special characters. If they do,
		 * escape the special characters.
		 */
		if (s == null)
			return s;
		
		String [][] specials;
        specials = new String[][]{
                {"\\",  "\\\\"},
                {"\0", "0"},
                {"'", "\\'"}, 
                {"\"",  "\\\""},
                {"\b",  "\\b"},
                {"\n",  "\\n"},
                {"\r",  "\\r"},
                {"\t",  "\\t"},
                {"\\Z", "\\\\Z"}, // not sure about this one
                {"%", "\\%"},     // used in searching
                {"_", "\\_"}
        };
        for (int i = 0; i < specials.length; i++) {
        	s = s.replace(specials[i][0], specials[i][1]);
        }
        
        return s;
	}
}
