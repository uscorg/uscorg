package models;

import java.sql.Timestamp;
//The Comment class contains basic information of a comment, all stored in String 
//with the exception of a Date Object. Storing the information in a String minimizes
//memory usage and a fetch operation from the database can be performed when necessary

public class Comment {
	private int commentID; //unique commentID from db
	private int announcementID;
	private String text; //the main text that a comment contains
	private int userID; //the authors name of the comment
	private Timestamp timeStamp; //the time when the comment is created
	
	//constructor
	public Comment(int id, int announcementID, String text, int auth, Timestamp time){
		commentID = id;
		this.announcementID = announcementID;
		this.text = text;
		this.userID = auth;
		timeStamp = time;
	}
	
	//toString function for easy printing
	@Override
	public String toString(){
		String toBePrinted = "";
		toBePrinted = text;
		toBePrinted = toBePrinted + "\n" + userID;
		toBePrinted = toBePrinted + "\n" + timeStamp.toString();
		toBePrinted = toBePrinted + "\n" + commentID;
		toBePrinted = toBePrinted + "\n" + announcementID;
		return toBePrinted;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getUser() {
		return userID;
	}
	public void setAuthor(int user) {
		this.userID = user;
	}
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getCommentID() {
		return commentID;
	}

	public void setCommentID(int commentID) {
		this.commentID = commentID;
	}
	
}
