package models;

import java.sql.Timestamp;
//The Announcement class contains basic information of a comment, all stored in String 
//with the exception of a Date Object. Storing the information in a String minimizes
//memory usage and a fetch operation from the database can be performed when necessary

public class Announcement {
	private int announcementID; //unique announcementID from db
	private int orgID; //the organization name that the announcement belongs to
	private int userID; //the authors name of the announcement
	private String text; //the main text that an announcement contains
	private String title; //the title of the announcement
	private Timestamp date; // the date when the announcement is posted
	
	//constructor
	public Announcement(int announcementID, int orgID, int userID, String text, String title, Timestamp date){
		this.announcementID = announcementID;
		this.orgID = orgID;
		this.userID = userID;
		this.text = text;
		this.title = title;
		this.date = date;
	}
	
	@Override
	public String toString(){
		String toBePrinted = "";
		toBePrinted = text;
		toBePrinted = toBePrinted + "\ntitle: " + title;
		toBePrinted = toBePrinted + "\nuserID:" + userID;
		toBePrinted = toBePrinted + "\ndate: " + date.toString();
		toBePrinted = toBePrinted + "\norgID: " + orgID;
		toBePrinted = toBePrinted + "\nannouncementID:" + announcementID;
		return toBePrinted;
	}
	
	//get set functions
	public int getOrg() {
		return orgID;
	}
	public void setOrg(int org) {
		this.orgID = org;
	}
	public int getUserID() {
		return userID;
	}
	public void setUser(int userID) {
		this.userID = userID;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}

	public int getAnnouncementID() {
		return announcementID;
	}

	public void setAnnouncementID(int announcementID) {
		this.announcementID = announcementID;
	}
	
}
