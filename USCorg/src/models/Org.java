package models;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

//The Org class contains information about an organization object.
//Data is stored in String and int.  Users and their access level is stored
//in a hashmap. Announcements and comments are stored in two Vectors.

public class Org {
	private String name; //name of the organization
	private String des; //description of the organization
	private String meetingTimes; //weekly meeting times of the organization, in dayTime format, e.g. M3pm-5pm.  Possible days are: M,T,W,Th,F,Sa,Su
	private String contact; //contact of organization, can be in any format, e.g. Calvin Leung 3127092231, 3127092231 Calvin, etcetc.
	private int memberNo; //number of members, just to keep track of the size of the organization, ******************remember to increment when adding new user
	private int dues; //dues every semester
	private String audience;//UG (UnderGrad) or G (Grad) or B (Both)
	private int orgID;

	//constructor with all variables
	public Org(String n, String d, String mt, String c, int mn, int du, String a, int id){
		name = n;
		des = d;
		meetingTimes = mt;
		contact = c;
		memberNo = mn;
		dues = du;
		audience = a;
		orgID = id;
	}
	//toString function for easy printing
	@Override
	public String toString(){
		String print = "orgName: ";
		print = print + name + "\nDes: " + des + "\nMT: " + meetingTimes + "\ncontact: " + contact + "\nmemberNo: " + memberNo + "\ndues: " + dues + "\naudience: " + audience;
		return print;
	}
	
	//get set functions
	public String getName(){
		return name;
	}
	public String getDes(){
		return des;
	}
	public String getMT(){
		if (meetingTimes.equals(null)){
			return "N/A";
		}
		return meetingTimes;
	}
	public String getContact(){
		return contact;
	}
	public int getMemberNo(){
		return memberNo;
	}

	public int getDues(){
		return dues;
	}
	public String getAudience(){
		if (audience.equals("B")){
			return "Both";
		} else if (audience.equals("U")) {
			return "Undergrad";
		} else if (audience.equals("G")) {
			return "Grad";
		} else {
			return audience;
		}
	}
	public void setName(String n){
		name = n;
	}
	public void setDes(String d){
		des = d;
	}
	public void setMT(String mt){
		meetingTimes = mt;
	}
	public void setContact(String c){
		contact = c;
	}
	public void setMemberNo(int m){
		memberNo = m;
	}
	public void setDues(int d){
		dues = d;
	}
	public void setAudience(String d){
		audience = d;
	}
	public int getOrgID() {
		return orgID;
	}
	public void setOrgID(int orgID) {
		this.orgID = orgID;
	}
}
