package models;

import java.awt.Toolkit;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.AnnouncementsThread;
import threads.CommentsThread;
import controllers.EventController;
import controllers.OrgController;
import controllers.UserController;

public class CurrentObjects {
	private static int verification = 0;
	private static Vector <String> ipList;
	private static User user; //current User object
	private static User almostRegisteredUser; //user to store data before text verification
	private static ExecutorService executor; //thread pool across the entire program
	private static int displayPanelWidth;
	private static CommentsThread commentsThread;
	private static AnnouncementsThread announcementsThread;

	static {
		setUser(UserController.makeGuestUser());
		ipList = new Vector<String>();
		executor = Executors.newCachedThreadPool();
	}
	private static Org org;
	static {
		setOrg(OrgController.makeTempOrg()); 
		
	}
	
	private static Announcement announcement;
	static {
		//currentAnnouncement = 
	}
	
	private static Event event;
	static {
		setEvent(EventController.makeTempEvent());
	}
	
	public static User getUser() {
		return user;
	}
	public static User getAlmostRegisteredUser() {
		return almostRegisteredUser;
	}
	public static void setUser(User user) {
		CurrentObjects.user = user;
	}
	public static void setAlmostRegisteredUser(User user) {
		CurrentObjects.almostRegisteredUser = user;
	}

	public static Org getOrg() {
		return org;
	}
	public static void setOrg(Org org) {
		CurrentObjects.org = org;
	}

	public static int getVerification() {
		return verification;
	}
	public static void setVerification(int veri) {
		verification = veri;

	}
	public static Event getEvent() {
		return event;
	}
	
	public static void setEvent(Event event) {
		CurrentObjects.event = event;
	}
	public static ExecutorService getExecutor() {
		return executor;
	}
	public static void setDisplayPanelWidth(int width) {
		CurrentObjects.displayPanelWidth = width;
	}
	public static int getDisplayPanelWidth() {
		return CurrentObjects.displayPanelWidth;
	}
	public static CommentsThread getCommentsThread() {
		return commentsThread;
	}
	public static AnnouncementsThread getAnnouncementsThread() {
		return announcementsThread;
	}
}
