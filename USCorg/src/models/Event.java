package models;

import java.sql.Date;
import java.sql.Timestamp;


public class Event {
	private int eventID;
	private int year;
	private int month;
	private int day;
	private String eventName;
	private Timestamp date_of;
	private Timestamp time;
	private String text;
	
	//Constructor
	public Event(int id, String text, String name, Timestamp time, Timestamp date_of, int year, int month, int day) {
		eventID = id;
		this.setYear(year);
		this.setMonth(month);
		this.setDay(day);
		setEventName(name);
		this.text = text;
		this.time = time;
		this.date_of = date_of;
	}
	
	public String toString(){
		return "\nName: "+eventName+"\nDate and Time: "+date_of.toString() + "\nEvent Content: " + text;
	}
	
	public int getEventID() {
		return eventID;
	}

	public void setEventID(int id) {
		eventID = id;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}


	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public Timestamp getDate_of() {
		return date_of;
	}

	public void setDate_of(Timestamp date_of) {
		this.date_of = date_of;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}
	
	public String getMonthString() {
		int m = getMonth();
		if (m == 1) {
			return "January";
		}
		if (m == 2) {
			return "February";
		}	
		if (m == 3) {
			return "March";
		}
		if (m == 4) {
			return "April";
		}
		if (m == 5) {
			return "May";
		}
		if (m == 6) {
			return "June";
		}
		if (m == 7) {
			return "July";
		}
		if (m == 8) {
			return "August";
		}
		if (m == 9) {
			return "September";
		}
		if (m == 10) {
			return "October";
		}
		if (m == 11) {
			return "November";
		}
		else {
			return "December";
		}
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
	//public String 

}
