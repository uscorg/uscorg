package database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controllers.UserController;



public abstract class Database {
	
	protected static Connection conn; //JDBC connection object
	protected static Statement st; //JDBC statement
	protected static PreparedStatement ps; //JDBC prepared statement 
	protected static ResultSet rs; //result set 
	//address,port,database name for remote connection
	protected static final String url = "jdbc:mysql://107.170.220.236:3306/UserDB"; 
	//database username 
	protected static final String user = "yuikwanl";
    //password associated with the username
	protected static final String pass = "USC.org";
    
    //initializer for JDBC variables and connection to db 
	static{
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url,user,pass);
			st = conn.createStatement();
		} catch (SQLException sqle) {
			System.out.println ("SQLException: " + sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			System.out.println ("ClassNotFoundException: " + cnfe.getMessage());
		}
	}

	public static void populate(){
		//UserDB.insertUser("1230802932", "nikita", "nikita", "500:a2911a1b76cc:bb347de182eb", "CS", 2016, "nikita");
		
		//EventDB.insertEvent(1, 2015, 05, 01, "haha1", "last day", 8);
		
		RSVP.insertRSVP(5, 1);
		RSVP.insertRSVP(6, 1);
		RSVP.insertRSVP(7, 1);
		RSVP.insertRSVP(8, 1);
		RSVP.insertRSVP(9, 1);
		
	}
	
 		/*insertUser("1230802932", "nikita", "nikita", "500:fb9311a62b75:165b1fa89b92", "CS", 2016, "nikita");*/
 		//AnnouncementDB.updateAnnouncement(3, "title2", "changed");
 		//EventDB.insertEvent(1, 2015, 05, 02, "last day", "last day", 8);
 		//System.out.println(OrgDB.searchOrgs("Club"));
 		//System.out.println(OrgDB.fetchOrgInACategory("Recreation"));
 		//EventDB.insertEvent(1, 2015, 05, 01, "haha", "last day", 8);
 		//EventDB.insertEvent(1, 2015, 05, 02, "haha", "last day", 8);
 		//RSVP.insertRSVP(2, 1);
 		//System.out.println(EventDB.fetchMostRecentEvent());
 		//System.out.println(OrgDB.searchOrgs("Club").size());
 		//System.out.println(OrgDB.searchOrgs("Club").elementAt(0));
 		//System.out.println(OrgDB.fetchOrgInACategory("Engineering").elementAt(0));
 		//System.out.println(OrgDB.fetchOrgInACategory("Engineering").elementAt(1));
 		//System.out.println(OrgDB.fetchOrgInACategory("Engineering").elementAt(0));
 		//System.out.println(OrgDB.fetchOrgInACategory("Engineering").elementAt(1));
 		/*insertAnnouncement(1,1,"title2","hi1");
 		insertAnnouncement(1,1,"title3","hi2");*/
 		//CommentDB.insertComment(1,1,"something");
 		//EventDB.insertEvent(1, 2015, 5, 14, "test", "test name", 5);
 		
 		//System.out.println(EventDB.fetchEvent(1).elementAt(0));
 		//System.out.println(EventDB.fetchEvent(1).elementAt(1));
}
