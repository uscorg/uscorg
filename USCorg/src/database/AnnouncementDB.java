package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Vector;

import models.Announcement;

public class AnnouncementDB extends Database{

	//returns true when an announcement is inserted 
	//takes orgID, userID, title and text as parameters
	public static boolean insertAnnouncement(int orgID, int userID, String title, String text){
		try {
			ps = conn.prepareStatement("INSERT INTO announcements (date_posted, userID, orgID, title, an_text) VALUES (NOW(), " + userID + "," + orgID + ", \'" + title + "\', \'" +text + "\');");
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	//returns a vector of announcements in an organization
	public static Vector <Announcement> fetchAnnouncement(int orgID) {
		Vector <Announcement> announcements = new Vector<Announcement>();
		int announcementID = 0;
		int userID = 0;
		String an_text = "";
		Timestamp timestamp = null;
		String title = "";
		
		boolean announcementExists = false;
		try {
			Connection conn; //JDBC connection object
			Statement st; //JDBC statement
			PreparedStatement ps; //JDBC prepared statement 
			ResultSet rs; //result set 
			conn = DriverManager.getConnection(url,user,pass);
			st = conn.createStatement();
			ps = conn.prepareStatement("SELECT * FROM announcements WHERE orgID=?");
			ps.setInt(1, orgID);
			rs = ps.executeQuery();
			while(rs.next()){
				title = rs.getString("title");
				announcementExists = true;
				userID = rs.getInt("userID");
				announcementID = rs.getInt("announcementID");
				an_text = rs.getString("an_text");
				timestamp = rs.getTimestamp("date_posted");
				announcements.add(new Announcement(announcementID, orgID, userID, an_text, title, timestamp));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (announcementExists){
			return announcements;
		}else{
			return null;
		}
		
	}
	public static boolean deleteAnnouncement(int announcementID){
		try {
			ps = conn.prepareStatement("DELETE FROM announcements WHERE announcementID=?");
			ps.setInt(1, announcementID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static boolean updateAnnouncement(int announcementID, String title, String text){
		try {
			ps = conn.prepareStatement("UPDATE announcements SET date_posted=NOW(), title=\'" + title + "\',an_text=\'" + text +"\' WHERE announcementID=?;");
			ps.setInt(1, announcementID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
