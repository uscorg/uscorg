package database;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import models.Announcement;
import models.Comment;
import models.Event;
import models.Org;
import models.User;

public class UserDB extends Database{
	//User functions
		//returns a username string when provided with the unique userid
		public static String fetchUsername(int id){
			String lname = "";
			String fname = "";
			String username = "";
			try {
				ps = conn.prepareStatement("SELECT * FROM users WHERE userID=?");
				ps.setInt(1, id);
				rs = ps.executeQuery();
				rs.next();
				username = rs.getString("uscName");
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return username;
		}
		
		//returns a studentID string when provided with the unique userid
		public static String fetchStudentID(int id){
			String studentid = "";
			try {
				ps = conn.prepareStatement("SELECT * FROM users WHERE userID=?");
				ps.setInt(1, id);
				rs = ps.executeQuery();
				rs.next();
				studentid = rs.getString("studentID");
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return studentid;
		}
		
		//returns a password string when provided with the unique userid
		public static String fetchpw(int id){
			String pw = "";
			try {
				
				ps = conn.prepareStatement("SELECT * FROM users WHERE userID=?");
				ps.setInt(1, id);
				rs = ps.executeQuery();
				rs.next();
				pw = rs.getString("pw");
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return pw;
		}
		
		//returns a major string when provided with the unique userid
		public static String fetchMajor(int id){
			String major = "";
			try {
				
				ps = conn.prepareStatement("SELECT * FROM users WHERE userID=?");
				ps.setInt(1, id);
				rs = ps.executeQuery();
				rs.next();
				major = rs.getString("major");
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return major;
		}
		
		//returns a user object when provided with a username string
		public static User fetchUserwithUsername(String username){
			int userID = 0;
			String studentID = "";
			String fname = "";
			String lname = "";
			String pw = ""; 
			String major = "";
			int yearOf = 0;
			String uscName = "";
			boolean userExists = false;
			try {
				
				ps = conn.prepareStatement("SELECT * FROM users WHERE uscName=?");
				ps.setString(1, username);
				rs = ps.executeQuery();
				while(rs.next()){
					userExists = true;
					userID = rs.getInt("userID");
					studentID = rs.getString("studentID");
					fname = rs.getString("fname");
					lname = rs.getString("lname");
					pw = rs.getString("pw"); 
					major = rs.getString("major");
					yearOf = rs.getInt("yearOf");
					uscName = rs.getString("uscName");
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (userExists){
				return new User(userID, studentID, fname, lname, pw, major, yearOf, uscName, true);
			}else{
				return null;
			}
			
		}
		
		//returns a vector of all the orgs from one single user
		public static Vector <Org> fetchUserOrganizations(int userID){
			Vector <Org> orgs = new Vector <Org> ();
			Vector <Integer> orgIDs = new Vector<Integer>();
			int orgID = 0;
			String name = "";
			String des = "";
			String meetingTimes = "";
			String contact = "";
			int memberNo = 0;
			int dues = 0;
			String audience = "";
			try {
				ps = conn.prepareStatement("SELECT * FROM Stu_Org WHERE userID=?");
				ps.setInt(1, userID);
				rs = ps.executeQuery();
				while (rs.next()){
					orgID = rs.getInt("orgID");
					orgIDs.add(orgID);
				}
				ps = conn.prepareStatement("SELECT * FROM org WHERE orgID=?");
				for (Integer orgid: orgIDs){
					ps.setInt(1, orgid);
					rs = ps.executeQuery();
					rs.next();
					name = rs.getString("orgName");
					des = rs.getString("des");
					meetingTimes = rs.getString("meetingTimes");
					contact = rs.getString("contact");
					memberNo = rs.getInt("memberNo");
					dues = rs.getInt("dues");
					audience = rs.getString("audience");
					orgID = rs.getInt("orgID");
					Org org = new Org(name, des, meetingTimes, contact, memberNo, dues, audience, orgID);
					orgs.add(org);
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return orgs;
		}
		
		//returns true if username is not used
		public static boolean usernameIsValid(String username){
			try {
				ps = conn.prepareStatement("SELECT * FROM users WHERE uscName=?");
				ps.setString(1, username);
				rs = ps.executeQuery();
				while(rs.next()){
					rs.close();
					ps.close();
					return false;
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		
		//returns true if uscID is not in database
		public static boolean idIsValid(String uscID){
					try {
						ps = conn.prepareStatement("SELECT * FROM users WHERE studentID=?");
						ps.setString(1, uscID);
						rs = ps.executeQuery();
						while(rs.next()){
							rs.close();
							ps.close();
							return false;
						}
						rs.close();
						ps.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return true;
			}
		
		//returns true if user is successfully added
		public static boolean insertUser(String studentID, String fname, String lname, String pw, String major, int yearOf, String uscName){
			try {
				ps = conn.prepareStatement("INSERT INTO users (studentID, fname, lname, pw, major, yearOf, uscName) VALUES (" + studentID + ", \'" + fname + "\', \'" + lname + "\', \'" + pw + "\', \'" + major + "\'," + yearOf + ", \'" + uscName + "\');" );
				ps.execute();
				rs.close();
				ps.close();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		//returns true if user is successfully updated
		public static boolean updateUser(String studentID, String fname, String lname, String pw, String major, int yearOf, String uscName){
			try {
				ps = conn.prepareStatement("UPDATE users SET studentID = \'" + studentID +"\', fname=\'" + fname +"\', lname=\'" + lname+"\', pw=\'" + pw+"\', major=\'" + major+"\', yearOf=\'" + yearOf+"\', uscName=\'" + uscName + "\' WHERE uscName=?;");
				ps.setString(1, uscName);
				ps.execute();
				rs.close();
				ps.close();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		//returns true if user's permission is successfully added to the db
		public static boolean insertPermission(int userID, int orgID, String permission){
			try {
				ps = conn.prepareStatement("INSERT INTO Stu_Org (userID, orgID, permission) VALUES (" + userID + ", + " + orgID + ", + \'" + permission + "\');");
				ps.execute();
				rs.close();
				ps.close();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
				
		//returns the permission of the user in the organization
		public static String getPermission(int userID, int orgID){
			try {
				ps = conn.prepareStatement("SELECT permission FROM Stu_Org WHERE userID=? AND orgID=?");
				ps.setInt(1, userID);
				ps.setInt(2, orgID);
				rs = ps.executeQuery();
				while (rs.next()){
					return rs.getString("permission");
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "Improper Permissions";
		}
		
		//change the permission of the user in the organization, returns 
		public static boolean updatePermission(int userID, int orgID, String permission){
			try {
				ps = conn.prepareStatement("UPDATE Stu_Org SET permission = \'" + permission + "\' WHERE userID=? AND orgID=?");
				ps.setInt(1, userID);
				ps.setInt(2, orgID);
				ps.execute();
				rs.close();
				ps.close();
				return true;

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
				return false;
			}
		}
		
		//returns true if user's ipAddress is successfully deleted from the db
		public static boolean deleteUserfromOrg(int userID, int orgID){
			try {
				ps = conn.prepareStatement("DELETE FROM Stu_Org WHERE userID=? AND orgID=?");
				ps.setInt(1, userID);
				ps.setInt(2,orgID);
				ps.execute();
				rs.close();
				ps.close();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
		
		//fetching a vector of events when the range of index is given
		public static Vector<Event> fetchUserRSVP(int userID){
			Vector <Event> events = new Vector<Event>();
			int eventID = 0;
			String event_text = "";
			Timestamp timestamp = null;
			Timestamp date_of = null;
			int year = 0;
			int month = 0;
			int day = 0;
			String event_name = "";
			Vector <Integer> eventIDs = new Vector <Integer> ();
			String an_text = "";
			
			boolean eventExists = false;
			try {
				ps = conn.prepareStatement("SELECT eventID FROM rsvp WHERE userID =?");
				ps.setInt(1, userID);
				rs = ps.executeQuery();
				while(rs.next()){
					eventExists = true;
					eventID = rs.getInt("eventID");
					eventIDs.add(eventID);
				}
				ps = conn.prepareStatement("SELECT * FROM events WHERE eventID =?");
				for(Integer id: eventIDs){
					ps.setInt(1, id);
					rs = ps.executeQuery();
					while(rs.next()){
						eventExists = true;
						eventID = rs.getInt("eventID");
						year = rs.getInt("year");
						month = rs.getInt("month");
						day = rs.getInt("day");
						event_text = rs.getString("event_text");
						timestamp = rs.getTimestamp("date_posted");
						date_of = rs.getTimestamp("date_of");
						event_name = rs.getString("event_name");
						
						events.add(new Event(eventID,event_text, event_name, timestamp, date_of, year, month, day));
					}
				}
				rs.close();
				ps.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (eventExists){
				return events;
			}else{
				return null;
			}
		}
		
		
}
