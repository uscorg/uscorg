package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Vector;

import models.Comment;

public class CommentDB extends Database{
	//comments function
	
	//inserting a comment when the announcementID, userID and the text is given
	public static boolean insertComment(int announcementID, int userID, String text){
		try {
			ps = conn.prepareStatement("INSERT INTO comments (date_posted, userID, announcementID, com_text) VALUES (NOW()," + userID + ", + " + announcementID + ", \'" + text + "\');");
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean insertEventComment(int announcementID, int userID, String text){
		try {
			ps = conn.prepareStatement("INSERT INTO eventComments (date_posted, userID, announcementID, com_text) VALUES (NOW()," + userID + ", + " + announcementID + ", \'" + text + "\');");
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//fetching a vector of comment when the range of index is given
	public static Vector <Comment> fetchComment(int announcementID){
		Vector <Comment> comments = new Vector<Comment>();
		int userID = 0;
		int commentID = 0;
		String an_text = "";
		Timestamp timestamp = null;
		
		boolean commentExists = false;
		try {
			ps = conn.prepareStatement("SELECT * FROM comments WHERE announcementID=? ORDER BY date_posted DESC");
			ps.setInt(1, announcementID);
			rs = ps.executeQuery();
			while(rs.next()){
				commentExists = true;
				userID = rs.getInt("userID");
				commentID = rs.getInt("commentID");
				an_text = rs.getString("com_text");
				timestamp = rs.getTimestamp("date_posted");
				comments.add(new Comment(commentID, announcementID, an_text, userID, timestamp));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (commentExists){
			return comments;
		}else{
			return null;
		}
	}
	
	//fetching a vector of comment when the range of index is given
	public static Vector <Comment> fetchEventComment(int announcementID){
		Vector <Comment> comments = new Vector<Comment>();
		int userID = 0;
		int commentID = 0;
		String an_text = "";
		Timestamp timestamp = null;
		
		boolean commentExists = false;
		try {
			ps = conn.prepareStatement("SELECT * FROM eventComments WHERE announcementID=? ORDER BY date_posted DESC");
			ps.setInt(1, announcementID);
			rs = ps.executeQuery();
			while(rs.next()){
				commentExists = true;
				userID = rs.getInt("userID");
				commentID = rs.getInt("commentID");
				an_text = rs.getString("com_text");
				timestamp = rs.getTimestamp("date_posted");
				comments.add(new Comment(commentID, announcementID, an_text, userID, timestamp));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (commentExists){
			return comments;
		}else{
			return null;
		}
	}
	
	//delete a single comment
	public static boolean deleteComment(int commentID){
		try {
			ps = conn.prepareStatement("DELETE FROM comments WHERE commentID=?;");
			ps.setInt(1, commentID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//delete a single comment
		public static boolean deleteEventComment(int commentID){
			try {
				ps = conn.prepareStatement("DELETE FROM eventComments WHERE commentID=?;");
				ps.setInt(1, commentID);
				ps.execute();
				ps.close();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
	
	//update a comment with commentID and text provided
	public static boolean updateComment(int commentID, String text){
		try {
			ps = conn.prepareStatement("UPDATE comments SET date_posted=NOW(), com_text=\'" + text +"\' WHERE commentID=?;");
			ps.setInt(1, commentID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean updateEventComment(int commentID, String text){
		try {
			ps = conn.prepareStatement("UPDATE eventComments SET date_posted=NOW(), com_text=\'" + text +"\' WHERE commentID=?;");
			ps.setInt(1, commentID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	

}
