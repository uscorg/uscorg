package database;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Vector;

import models.Event;
import models.User;

public class RSVP extends Database{
	//inserts a rsvp that matches a user with an event
	public static boolean insertRSVP(int eventID, int userID){
		try {
			ps = conn.prepareStatement("INSERT INTO rsvp (userID, eventID) VALUES (" + userID +","+eventID+");");
			ps.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//fetching a vector of events when the range of index is given
	public static Vector<User> fetchRSVPUsers(int eventID){
		Vector <User> users = new Vector<User>();
		Vector <Integer> userIDs = new Vector <Integer> ();
		String studentID = "";
		String fname = "";
		String lname = "";
		String pw = ""; 
		String major = "";
		int yearOf = 0;
		String uscName = "";
		int userID = 0;
		String an_text = "";
		Timestamp timestamp = null;
		
		boolean userExists = false;
		try {
			ps = conn.prepareStatement("SELECT userID FROM users WHERE userID =?");
			ps.setInt(1, userID);
			rs = ps.executeQuery();
			while(rs.next()){
				userExists = true;
				userID = rs.getInt("userID");
				userIDs.add(userID);
			}
			ps = conn.prepareStatement("SELECT * FROM events WHERE eventID =?");
			for(Integer id: userIDs){
				ps.setInt(1, id);
				rs = ps.executeQuery();
				while(rs.next()){
					userExists = true;
					userID = rs.getInt("userID");
					studentID = rs.getString("studentID");
					fname = rs.getString("fname");
					lname = rs.getString("lname");
					pw = rs.getString("pw"); 
					major = rs.getString("major");
					yearOf = rs.getInt("yearOf");
					uscName = rs.getString("uscName");
					users.add(new User(userID, studentID, fname, lname, pw, major, yearOf, uscName, true));
				}
			}

			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (userExists){
			return users;
		}else{
			return null;
		}
	}
	
	//delete an rsvp, deleting the user from the 
	public static boolean deleteRSVP(int userID){
		try {
			ps = conn.prepareStatement("DELETE FROM rsvp WHERE userID=?;");
			ps.setInt(1, userID);
			ps.execute();
			rs.close();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static Vector<Event> fetchMostRecentRSVP(int userID){
		Vector<Event> events = new Vector<Event>();
		int eventID = 0;
		String event_text = "";
		Timestamp timestamp = null;
		Timestamp date_of;
		int year = 0;
		int month = 0;
		int day = 0;
		String event_name = "";
		Event tempEvent = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM orgEvents JOIN rsvp ON rsvp.eventID=orgEvents.eventID WHERE rsvp.userID=? ORDER BY date_of ASC LIMIT 5 ;");
			ps.setInt(1, userID);
			rs = ps.executeQuery();
			
			while(rs.next()){
				
				eventID = rs.getInt("eventID");
				year = rs.getInt("year");
				month = rs.getInt("month");
				day = rs.getInt("day");
				event_text = rs.getString("event_text");
				date_of = rs.getTimestamp("date_of");
				timestamp = rs.getTimestamp("date_posted");
				event_name = rs.getString("event_name");
				
				tempEvent = new Event(eventID,event_text, event_name, timestamp, date_of, year, month, day);
				events.add(tempEvent);
			}
			rs.close();
			ps.close();
			return events;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
