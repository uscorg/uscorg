package database;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.Format;
import java.util.Vector;

import models.Comment;
import models.Event;

public class EventDB extends Database{
	public static boolean insertEvent(int orgID, int year, int month, int day, String text, String name, int time){
		long seconds = ((long)year-1970)*(long)Math.pow(3.15569, 10) + ((long)month-1)*(long)Math.pow(2.62974, 9) + ((long)day-1)*86400000;
		String temp = "0001-01-01 00:00:00";
	    
		String date = ""+year+"-"+month+"-" + day + " " + time + ":00:00";
		Timestamp ts = Timestamp.valueOf(date);
		try {
			ps = conn.prepareStatement("INSERT INTO orgEvents (date_posted, date_of, orgID, event_text, year, month, day, event_name) VALUES (NOW(),?,?,?,?,?,?,?);" );
			ps.setTimestamp(1, ts);
			ps.setInt(2, orgID);
			ps.setString(3, text);
			ps.setInt(4, year);
			ps.setInt(5, month);
			ps.setInt(6, day);
			ps.setString(7, name);
			
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	//fetching a vector of comment when the range of index is given
	public static Vector <Event> fetchEvent(int orgID){
		Vector <Event> events = new Vector<Event>();
		int eventID = 0;
		String event_text = "";
		Timestamp timestamp = null;
		Timestamp date_of;
		int year = 0;
		int month = 0;
		int day = 0;
		String event_name = "";
		
		boolean eventExists = false;
		try {
			ps = conn.prepareStatement("SELECT * FROM orgEvents WHERE orgID=? ORDER BY date_of DESC");
			ps.setInt(1, orgID);
			rs = ps.executeQuery();
			while(rs.next()){
				eventExists = true;
				eventID = rs.getInt("eventID");
				year = rs.getInt("year");
				month = rs.getInt("month");
				day = rs.getInt("day");
				event_text = rs.getString("event_text");
				date_of = rs.getTimestamp("date_of");
				timestamp = rs.getTimestamp("date_posted");
				event_name = rs.getString("event_name");
				
				events.add(new Event(eventID,event_text, event_name, timestamp, date_of, year, month, day));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (eventExists){
			return events;
		}else{
			return null;
		}
	}
	
	//delete a single event
	public static boolean deleteEvent(int eventID){
		try {
			ps = conn.prepareStatement("DELETE FROM orgEvents WHERE eventID=?;");
			ps.setInt(1, eventID);
			ps.execute();
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	

}
