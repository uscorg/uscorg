package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import models.Announcement;
import models.Comment;
import models.Org;
import utils.Pair;

public class OrgDB extends Database{
	//Org Functions
	//Search and returns a vector of <orgName, description> with a keyword
 	public static Vector< Org > searchOrgs (String keyword ){
 		keyword = "%"+keyword + "%";
 		Vector<Org> orgs = new Vector <Org>();
		String name = "";
		String des = "";
		String meetingTimes = "";
		String contact = "";
		int memberNo = 0;
		int dues = 0;
		int orgID = 0;
		String audience = "";
		try {
			conn = DriverManager.getConnection(url,user,pass);
			ps = conn.prepareStatement("SELECT * FROM org WHERE orgName LIKE ?;");
			ps.setString(1, keyword);
			rs = ps.executeQuery();
			while (rs.next()){
				name = rs.getString("orgName");
				des = rs.getString("des");
				meetingTimes = rs.getString("meetingTimes");
				contact = rs.getString("contact");
				memberNo = rs.getInt("memberNo");
				dues = rs.getInt("dues");
				audience = rs.getString("audience");
				orgID = rs.getInt("orgID");
				orgs.add(new Org(name, des, meetingTimes, contact, memberNo, dues, audience, orgID));
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orgs;
	}
 	public static Org fetchOrgwithOrgID(int orgID){
		String name = "";
		String des = "";
		String meetingTimes = "";
		String contact = "";
		int memberNo = 0;
		int dues = 0;
		String audience = "";
		Org org=null;
		try {
			ps = conn.prepareStatement("SELECT * FROM org WHERE orgID=?");
			ps.setInt(1, orgID);
			rs = ps.executeQuery();
			rs.next();
			name = rs.getString("orgName");
			des = rs.getString("des");
			meetingTimes = rs.getString("meetingTimes");
			contact = rs.getString("contact");
			memberNo = rs.getInt("memberNo");
			dues = rs.getInt("dues");
			audience = rs.getString("audience");
			orgID = rs.getInt("orgID");
			org = new Org(name, des, meetingTimes, contact, memberNo, dues, audience, orgID);
		rs.close();
		ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return org;
	}
	//returns a organization object when provided with the unique orgid
 	public static Org fetchOrgwithOrgname(String orgName){
		String name = "";
		String des = "";
		String meetingTimes = "";
		String contact = "";
		int memberNo = 0;
		int dues = 0;
		int orgID = 0;
		String audience = "";
		try {
			ps = conn.prepareStatement("SELECT * FROM org WHERE orgName=?");
			ps.setString(1, orgName);
			rs = ps.executeQuery();
			rs.next();
			name = rs.getString("orgName");
			des = rs.getString("des");
			meetingTimes = rs.getString("meetingTimes");
			contact = rs.getString("contact");
			memberNo = rs.getInt("memberNo");
			dues = rs.getInt("dues");
			audience = rs.getString("audience");
			orgID = rs.getInt("orgID");
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Org org = new Org(name, des, meetingTimes, contact, memberNo, dues, audience, orgID);
		return org;
	}
	
 	//returns an orgName string when provided with the unique orgid
	public static String fetchOrgName(int id){
		String name = "";
		try {
			ps = conn.prepareStatement("SELECT * FROM org WHERE orgID=?");
			ps.setInt(1, id);
			rs = ps.executeQuery();
			rs.next();
			name = rs.getString("orgName");
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return name;
	}
 	
	//returns true if orgName is not used
	public static boolean orgNameIsValid(String orgName){
			try {
				ps = conn.prepareStatement("SELECT * FROM org WHERE orgName=?");
				ps.setString(1, orgName);
				rs = ps.executeQuery();
				while(rs.next()){
					return false;
				}
				rs.close();
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
	
	//returns true if org is inserted
	public static boolean insertOrg(String orgName, String des, String mt, String contact, int memberNo, int dues, String audience){
		try {
			ps = conn.prepareStatement("INSERT INTO org(orgName, des, meetingTimes, contact, memberNo, dues, audience) VALUES (?,?,?,?,?,?,?);");
			ps.setString(1, orgName);
			ps.setString(2, des);
			ps.setString(3, mt);
			ps.setString(4, contact);
			ps.setInt(5, memberNo);
			ps.setInt(6,dues);
			ps.setString(7, audience);
			ps.execute();
			
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean updateOrg(String orgName, String des, String mt, String contact, int memberNo, int dues, String audience, int orgID){
		try {
			ps = conn.prepareStatement("UPDATE org SET orgName=?, des=?, meetingTimes=?, contact=?, memberNo=?, dues=?, audience=? WHERE orgID=?;");
			ps.setString(1, orgName);
			ps.setString(2, des);
			ps.setString(3, mt);
			ps.setString(4, contact);
			ps.setInt(5, memberNo);
			ps.setInt(6,dues);
			ps.setString(7, audience);
			ps.setInt(8,orgID);
			ps.execute();
			
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean insertOrgCategory(int orgID, int keywordID){
		try {
			ps = conn.prepareStatement("INSERT INTO org_keywords (keywordID, orgID) VALUES ("+keywordID + ","+orgID +");");
			ps.execute();
			
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean deleteOrgCategory(int orgID, String keyword){
		try {
			ps = conn.prepareStatement("DELETE FROM org_keywords WHERE orgID=? AND keyword=?;");
			ps.setInt(1, orgID);
			ps.setString(2, keyword);
			ps.execute();
			
			ps.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public static Vector<String> fetchOrgCategories(int orgID){
		Vector<String> categories = new Vector <String>();
		String keyword;
		try {
			ps = conn.prepareStatement("SELECT org_keywords.orgID, keywords.title FROM org_keywords INNER JOIN keywords ON org_keywords.keywordID=keywords.keywordID WHERE org_keywords.orgID=?;");
			ps.setInt(1, orgID);
			
			rs = ps.executeQuery();
			while (rs.next()){
				keyword = rs.getString("title");
				categories.add(keyword);
			}
			
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return categories;
	}
	
	public static Vector<Org> fetchOrgInACategory(String category){
		Vector<Integer> orgIDs = new Vector<Integer> ();
		String name = "";
		String des = "";
		String meetingTimes = "";
		String contact = "";
		int memberNo = 0;
		int dues = 0;
		String audience = "";
		int orgID = 0;
		Vector<Org> orgs = new Vector <Org>();
		try {
			ps = conn.prepareStatement("SELECT * FROM org_keywords WHERE keyword=?;");
			ps.setString(1, category);
			rs = ps.executeQuery();
			while(rs.next()){
				orgID = rs.getInt("orgID");
				orgIDs.add(orgID);
			}
			ps = conn.prepareStatement("SELECT * FROM org WHERE orgID=?;");
			for (Integer a:orgIDs){
				ps.setInt(1, a);
				rs = ps.executeQuery();
				while(rs.next()){
					orgID = rs.getInt("orgID");
					name = rs.getString("orgName");
					des = rs.getString("des");
					meetingTimes = rs.getString("meetingTimes");
					contact = rs.getString("contact");
					memberNo = rs.getInt("memberNo");
					dues = rs.getInt("dues");
					audience = rs.getString("audience");
					orgID = rs.getInt("orgID");
					orgs.add(new Org(name, des, meetingTimes, contact, memberNo, dues, audience, orgID));
				}
				
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orgs;
	}
	
	public static boolean orgCategoryIsValid(int orgID, String keyword){
		boolean ok = true;
		try {
			ps = conn.prepareStatement("SELECT * FROM org_keywords WHERE orgID=?;");
			ps.setInt(1, orgID);
			rs = ps.executeQuery();
			while (rs.next()){
				String temp = rs.getString("keyword");
				if (temp.equals(keyword)){
					ok = false;
				}
			}
			rs.close();
			ps.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ok;
	}
	
}
