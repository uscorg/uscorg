package threads;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import views.DisplayPanel;
import controllers.CommentController;
import controllers.UserController;
import models.Comment;
import models.CurrentObjects;

public class CommentsThread extends Thread {
	private int announcementID;
	private JScrollPane commentScrollPane;
	private JPanel commentMainPanel;
	private boolean needToRefresh = true;
	private String lastComment = "";
	private boolean needToStop = false;
	private DisplayPanel displayPanel;
	private boolean checker;
	
	public CommentsThread(int announcementID, JScrollPane commentScrollPane, JPanel commentMainPanel, DisplayPanel displayPanel) {
		this.announcementID = announcementID;
		this.commentMainPanel = commentMainPanel;
		this.commentScrollPane = commentScrollPane;
		this.displayPanel = displayPanel;
		this.displayPanel.switchBool(); //this is required because the normal swap happens
										//AFTER the thread starts executing
		this.checker = this.displayPanel.getBool();
	}
	
	public void run() {

		if (displayPanel.getBool() != checker) {
			//stopping
			needToStop = true;
		} else {
			//still the same page
			Vector<Comment> commentsVector = CommentController.getComment(announcementID);
			if (commentsVector == null) {
				//no comments
				commentMainPanel.removeAll();
				commentMainPanel.add(new JLabel("No comments for this announcement. Be the first to add one!"));
				commentMainPanel.revalidate();
				commentMainPanel.repaint();
				commentScrollPane.revalidate();
				commentScrollPane.repaint();
				commentScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			} else if (lastComment.equals(commentsVector.get(commentsVector.size()-1).getText())) {
				//last comment is the same as the last comment in the vector
				//therefore, no refresh is needed
				needToRefresh = false;
			} else {
				//otherwise, we do need to refresh!
				needToRefresh = true;
			}
			if (needToRefresh) {
				//comments present + need to refresh
				lastComment = commentsVector.get(commentsVector.size()-1).getText();
				commentMainPanel.removeAll();
				
				JPanel [] commentPanels = new JPanel[commentsVector.size()];
				JLabel [] authorLabels = new JLabel[commentsVector.size()];
				JTextArea [] commentLabels = new JTextArea[commentsVector.size()];
				JLabel [] timeLabels = new JLabel[commentsVector.size()];

				for (int i = 0; i < commentsVector.size(); i++) {
					commentPanels[i] = new JPanel(new BorderLayout());
					commentPanels[i].setBorder(BorderFactory.createEtchedBorder());

					authorLabels[i] = new JLabel();
					authorLabels[i].setText(UserController.getUser(commentsVector.get(i).getUser()).getFirstName() + " " +
							UserController.getUser(commentsVector.get(i).getUser()).getLastName() + " said:");

					authorLabels[i].setFont(new Font("Garamond", Font.BOLD, 20));	

					commentLabels[i] = new JTextArea();
					commentLabels[i].setText("\"" + commentsVector.get(i).getText() + "\"");
					commentLabels[i].setFont(new Font("Garamond", Font.ITALIC, 16));
					commentLabels[i].setBorder(BorderFactory.createEmptyBorder(3, 0, 5, 0));
					commentLabels[i].setEditable(false);
					commentLabels[i].setLineWrap(true);
					commentLabels[i].setWrapStyleWord(true);
					commentLabels[i].setFocusable(false);
					commentLabels[i].setOpaque(false);
					
					timeLabels[i] = new JLabel();
					timeLabels[i].setText("" + new Date(commentsVector.get(i).getTimeStamp().getTime()));
					timeLabels[i].setFont(new Font("Garamond", Font.PLAIN, 12));	
					
					commentPanels[i].add(authorLabels[i], BorderLayout.NORTH);
					commentPanels[i].add(commentLabels[i], BorderLayout.CENTER);
					commentPanels[i].add(timeLabels[i], BorderLayout.SOUTH);
				
					commentMainPanel.add(commentPanels[i]);
				}
				commentMainPanel.revalidate();
				commentMainPanel.repaint();
				commentScrollPane.revalidate();
				commentScrollPane.repaint();
				commentScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			}
		}

		try {
			if (!needToStop) {
				sleep(5000);
				run();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void signalStop() {
		needToStop = true;
	}
			
}
