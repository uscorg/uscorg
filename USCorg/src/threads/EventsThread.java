package threads;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import java.util.concurrent.ExecutorService;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import models.Event;
import views.EventPanel;
import views.DisplayPanel;
import controllers.EventController;

public class EventsThread extends Thread{
	private DisplayPanel displayPanel;
	private JLabel [] EventTitle;
	private JLabel [] EventText;
	private JPanel EventsPanel;
	private JScrollPane EventscrollPane;
	private int orgID;
	private boolean needToStop = false;
	private ExecutorService executor;
	private boolean checker;
	
	public EventsThread(JLabel [] EventTitle, JLabel[] EventText, JPanel EventsPanel, JScrollPane EventscrollPane, int orgID, DisplayPanel displayPanel) {
		this.displayPanel = displayPanel;
		this.EventTitle = EventTitle;
		this.EventText = EventText;
		this.EventsPanel = EventsPanel;
		this.EventscrollPane = EventscrollPane;
		this.orgID = orgID;
		this.displayPanel.switchBool(); //this is required because the normal swap happens
										//AFTER the thread starts executing
		this.checker = this.displayPanel.getBool();
	}
	
	public void run() {
		/*
		 * Pulls the Events that are shown on the panel + the ones in DB
		 * Then compares the last one of each, and, if the Events' texts are
		 * the same, no update is needed. Otherwise, update the shown Events
		 */
		
		if (displayPanel.getBool() == checker) {
			//need to refresh, still on the same page
			loadEvents();
		} else {
			//stopping
			needToStop = true;
		}
		
		
		try {
			if(!needToStop) {
				//does the thread need to keep running?
				sleep(5000);
				run();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void loadEvents() {
		Vector<Event> EventsVector = EventController.getEvent(orgID);
		if (EventsVector != null) {
			//Events present
			EventTitle = new JLabel[EventsVector.size()];
			EventText = new JLabel[EventsVector.size()];

			EventsPanel.removeAll();
			if (EventsVector.size() < 5) {
				EventsPanel.setLayout(new GridLayout(5, 1));
			} else {
				EventsPanel.setLayout(new GridLayout(EventsVector.size(), 1));
			}
			EventsPanel.setBorder(BorderFactory.createTitledBorder("Events"));
			for (int i = 0; i < EventsVector.size(); i++) {
				final JPanel singlePanel;
				singlePanel = new JPanel();
				singlePanel.setLayout(new BorderLayout());
				EventTitle[i] = new JLabel();
				EventTitle[i].setText(EventsVector.get(EventsVector.size()-i-1).getEventName());
				EventTitle[i].setFont(new Font("Garamond", Font.BOLD, 18));
				EventText[i] = new JLabel();
				EventText[i].setPreferredSize(new Dimension((int) EventText[i].getPreferredSize().getHeight(), EventsPanel.getWidth()));
				EventText[i].setText(EventsVector.get(EventsVector.size()-i-1).getText());
				EventText[i].setFont(new Font("Garamond", Font.ITALIC, 12));
				
				final String title = EventTitle[i].getText();
				singlePanel.add(EventTitle[i], BorderLayout.NORTH);
				singlePanel.add(EventText[i], BorderLayout.CENTER);
				singlePanel.setBorder(BorderFactory.createRaisedBevelBorder());
				singlePanel.addMouseListener(new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
					}

					public void mouseEntered(MouseEvent e) {
					}

					public void mouseExited(MouseEvent e) {
					}
					public void mousePressed(MouseEvent e) {
						//simulate a button look
						singlePanel.setBorder(BorderFactory.createLoweredBevelBorder());
					}
					public void mouseReleased(MouseEvent e) {
						//change the screen + signal the thread to stop
						singlePanel.setBorder(BorderFactory.createRaisedBevelBorder());
						displayPanel.swap(new EventPanel(orgID, title, displayPanel));
						needToStop = true;
					}
				});
				EventsPanel.add(singlePanel);
			}
			EventsPanel.revalidate();
			EventsPanel.repaint();
			EventsPanel.setPreferredSize(new Dimension(150, EventsPanel.getMinimumSize().height));
			EventscrollPane.revalidate();
			EventscrollPane.repaint();
			EventscrollPane.setPreferredSize(new Dimension(150,150));
			EventscrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		} else {
			//no Events!
			EventsPanel.removeAll();
			EventsPanel.add(new JLabel("No Events found!"));
			EventsPanel.revalidate();
			EventsPanel.repaint();
		}
	}

}
