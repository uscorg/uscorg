package threads;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import java.util.concurrent.ExecutorService;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import models.Announcement;
import controllers.AnnouncementController;
import views.AnnouncementPanel;
import views.DisplayPanel;
import views.OrganizationPanel;

public class AnnouncementsThread extends Thread {
	private DisplayPanel displayPanel;
	private JLabel [] announcementTitle;
	private JLabel [] announcementText;
	private JPanel announcementsPanel;
	private JScrollPane announcementScrollPane;
	private int orgID;
	private boolean needToStop = false;
	private ExecutorService executor;
	private boolean checker;
	
	public AnnouncementsThread(JLabel [] announcementTitle, JLabel[] announcementText, JPanel announcementsPanel, JScrollPane announcementScrollPane, int orgID, DisplayPanel displayPanel) {
		this.displayPanel = displayPanel;
		this.announcementTitle = announcementTitle;
		this.announcementText = announcementText;
		this.announcementsPanel = announcementsPanel;
		this.announcementScrollPane = announcementScrollPane;
		this.orgID = orgID;
		this.displayPanel.switchBool(); //this is required because the normal swap happens
										//AFTER the thread starts executing
		this.checker = !this.displayPanel.getBool();
	}
	
	public void run() {
		/*
		 * Pulls the announcements that are shown on the panel + the ones in DB
		 * Then compares the last one of each, and, if the announcements' texts are
		 * the same, no update is needed. Otherwise, update the shown announcements
		 */
		try {
			sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		if (displayPanel.getBool() == checker) {
			//need to refresh, still on the same page
			loadAnnouncements();
		} else {
			//stopping
			needToStop = true;
		}

		try {
			if(!needToStop) {
				//does the thread need to keep running?
				sleep(5000);
				run();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void signalStop() {
		needToStop = true;
	}
	
	private void loadAnnouncements() {
		Vector<Announcement> announcementsVector = AnnouncementController.getAnnouncement(orgID);
		if (announcementsVector != null) {
			//announcements present
			announcementTitle = new JLabel[announcementsVector.size()];
			announcementText = new JLabel[announcementsVector.size()];

			announcementsPanel.removeAll();
			if (announcementsVector.size() < 5) {
				announcementsPanel.setLayout(new GridLayout(5, 1));
			} else {
				announcementsPanel.setLayout(new GridLayout(announcementsVector.size(), 1));
			}
			announcementsPanel.setBorder(BorderFactory.createTitledBorder("Announcements"));
			for (int i = 0; i < announcementsVector.size(); i++) {
				final JPanel singlePanel;
				singlePanel = new JPanel();
				singlePanel.setLayout(new BorderLayout());
				announcementTitle[i] = new JLabel();
				announcementTitle[i].setText(announcementsVector.get(announcementsVector.size()-i-1).getTitle());
				announcementTitle[i].setFont(new Font("Garamond", Font.BOLD, 18));
				announcementText[i] = new JLabel();
				announcementText[i].setPreferredSize(new Dimension((int) announcementText[i].getPreferredSize().getHeight(), announcementsPanel.getWidth()));
				announcementText[i].setText(announcementsVector.get(announcementsVector.size()-i-1).getText());
				announcementText[i].setFont(new Font("Garamond", Font.ITALIC, 12));
				
				final String title = announcementTitle[i].getText();
				singlePanel.add(announcementTitle[i], BorderLayout.NORTH);
				singlePanel.add(announcementText[i], BorderLayout.CENTER);
				singlePanel.setBorder(BorderFactory.createRaisedBevelBorder());
				singlePanel.addMouseListener(new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
					}

					public void mouseEntered(MouseEvent e) {
					}

					public void mouseExited(MouseEvent e) {
					}
					public void mousePressed(MouseEvent e) {
						//simulate a button look
						singlePanel.setBorder(BorderFactory.createLoweredBevelBorder());
					}
					public void mouseReleased(MouseEvent e) {
						//change the screen + signal the thread to stop
						singlePanel.setBorder(BorderFactory.createRaisedBevelBorder());
						displayPanel.swap(new AnnouncementPanel(orgID, title, displayPanel));
						needToStop = true;
					}
				});
				announcementsPanel.add(singlePanel);
			}
			announcementsPanel.revalidate();
			announcementsPanel.repaint();
			announcementsPanel.setPreferredSize(new Dimension(150, announcementsPanel.getMinimumSize().height));
			announcementScrollPane.revalidate();
			announcementScrollPane.repaint();
			announcementScrollPane.setPreferredSize(new Dimension(150,150));
			announcementScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		} else {
			//no announcements!
			announcementsPanel.removeAll();
			announcementsPanel.add(new JLabel("No announcements found!"));
			announcementsPanel.revalidate();
			announcementsPanel.repaint();
		}
	}
}
