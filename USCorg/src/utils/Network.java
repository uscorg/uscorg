package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;


//A class to store if there is network connection 
public class Network {
	private static boolean connected;
	private static String ipAddress;
	
	static{
		URL toCheckIp;
		try {
			toCheckIp = new URL("http://checkip.amazonaws.com");
			
			BufferedReader in = new BufferedReader(new InputStreamReader(toCheckIp.openStream()));
			setIpAddress(in.readLine());
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			setIpAddress("Error, no connection");
			setConnected(false);
		}
		
	}

	public static boolean isConnected() {
		return connected;
	}

	public static void setConnected(boolean connected) {
		Network.connected = connected;
	}

	public static String getIpAddress() {
		return ipAddress;
	}

	public static void setIpAddress(String ipAddress) {
		Network.ipAddress = ipAddress;
	}
	
}
