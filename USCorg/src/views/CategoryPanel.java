package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import database.OrgDB;
import models.Org;

public class CategoryPanel extends JPanel implements TreeSelectionListener {
	
	private JTree organizationTree;
	private ArrayList<String> categoryArray;
	private JLabel categoryLabel;
	private DisplayPanel displayPanel;
		//We need to meet in person in order to sort this shit out but I basically made an arraylist
	//of categories
	//I'm assuming we'll then use this to add into each thing as I did underneath manually -Priyam
	public CategoryPanel(DisplayPanel displayPanel)
	{
		this.displayPanel = displayPanel;
		categoryArray = new ArrayList<String>();
		categoryArray.add("Recreation");
		categoryArray.add("Academic");
		categoryArray.add("Performance");
		categoryArray.add("Cultural");
		categoryArray.add("Greek");
		categoryArray.add("Political");
		categoryArray.add("Religious/Spiritual");
		categoryArray.add("Residential");
		categoryArray.add("Service");
		categoryArray.add("Social");
		categoryLabel = new JLabel("  View Organizations (By Category) \n \n \n");
		Vector <Org> temp= new Vector<Org>();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(" Category");
		for(int i = 0; i < categoryArray.size(); i++)
		{
			DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(categoryArray.get(i));
			if(i == 0)
			{
				temp = OrgDB.fetchOrgInACategory("Recreation");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if(i == 1)
			{
				temp = OrgDB.fetchOrgInACategory("Academic");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if(i == 2)
			{
				temp = OrgDB.fetchOrgInACategory("Performance");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 3){
				temp = OrgDB.fetchOrgInACategory("Cultural");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 4){
				temp = OrgDB.fetchOrgInACategory("Greek");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if(i == 5){
				temp = OrgDB.fetchOrgInACategory("Political");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 6){
				temp = OrgDB.fetchOrgInACategory("Religious/Spiritual");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 7) {
				temp = OrgDB.fetchOrgInACategory("Residential");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 8) {
				temp = OrgDB.fetchOrgInACategory("Service");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			if (i == 9){
				temp = OrgDB.fetchOrgInACategory("Social");
				for (int j = 0; j < temp.size(); ++j){
					categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
				}
			}
			root.add(categoryNode);
		}
		
		
		setBackground(Color.WHITE);
		setLayout(new BorderLayout());
		organizationTree = new JTree(root);
		organizationTree.setBackground(Color.WHITE);
		organizationTree.setFont(new Font("Garamond", Font.BOLD, 14));
		organizationTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		organizationTree.addTreeSelectionListener(this);
		
		//organizationTree.
		add(categoryLabel, BorderLayout.NORTH);
		add(organizationTree, BorderLayout.CENTER);
		add(new JLabel(" "),BorderLayout.SOUTH);
		add(new JLabel(" "),BorderLayout.SOUTH);
		add(new JLabel(" "),BorderLayout.SOUTH);


	}
	public void valueChanged(TreeSelectionEvent arg0) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) organizationTree.getLastSelectedPathComponent();
		if(node == null)
			return;
		Object nodeInfo = node.getUserObject();
		
		if(node.isLeaf()) {
			String temp = nodeInfo.toString();
			JScrollPane orgScroll = new JScrollPane();
			Org tempOrg = OrgDB.fetchOrgwithOrgname(temp);
			orgScroll = new JScrollPane(new OrganizationPanel(tempOrg.getOrgID(), displayPanel));
			displayPanel.swap(orgScroll);
		} 
		
	}
}
