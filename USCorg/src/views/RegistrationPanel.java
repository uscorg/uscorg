package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import models.CurrentObjects;
import controllers.UserController;

/* Registration panel that will go in place of the central
 * display panel in the GUI. Shows up when a user clicks
 * the "Click to Register" button.
 */

public class RegistrationPanel extends JPanel {
	
	JButton registerButton = new JButton ("Register");
	JTextField firstNameTextField = new JTextField();
	JTextField lastNameTextField = new JTextField();
	JTextField emailTextField = new JTextField();
	JTextField idTextField = new JTextField();
	JPasswordField passwordTextField = new JPasswordField();
	JPasswordField confirmPasswordTextField = new JPasswordField();
	JTextField majorTextField = new JTextField();
	JTextField phoneTextField = new JTextField();
	JComboBox yearBox = new JComboBox(new String[]{"2015","2016","2017","2018","2019"});

	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	DisplayPanel orgPanel;
	
	public RegistrationPanel( DisplayPanel displayPanel, DisplayPanel userPanel, DisplayPanel orgPanel)  {
		registerButton.setFont(new Font("Garamond", Font.BOLD, 14));
		firstNameTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		lastNameTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		emailTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		idTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		passwordTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		confirmPasswordTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		majorTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		phoneTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		yearBox.setFont(new Font("Garamond", Font.BOLD, 14));
		this.displayPanel = displayPanel;
		this.userPanel = userPanel;
		this.orgPanel = orgPanel;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.white);
		JLabel titleLabel = new JLabel("        Registration: Page 1                             \n");
		titleLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		titleLabel.setForeground(Color.BLUE);
		JLabel blankLabel = new JLabel("        Personal Information    ");
		blankLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		blankLabel.setForeground(Color.RED);
		add(titleLabel);
		add(blankLabel);
		add(getRegistrationPanel());
		
	}
	
	JPanel getRegistrationPanel() {
		mainPanel.setLayout(new GridLayout(17,2));
		mainPanel.setBackground(Color.white);
		
		JLabel firstnameLabel = new JLabel("    First Name: ");
		firstnameLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel lastnameLabel = new JLabel("    Last Name: ");
		lastnameLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel emailLabel = new JLabel("    USC Email: ");
		emailLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel idLabel = new JLabel("    USC ID #: ");
		idLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel passwordLabel = new JLabel("     New Password: ");
		passwordLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel confirmPasswordLabel = new JLabel("     Confirm Password: ");
		confirmPasswordLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel majorLabel = new JLabel("     Major(s): ");
		majorLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel phoneLabel = new JLabel("     Phone Number: ");
		phoneLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		//JTextField yearTextField = new JTextField("Confirm Password");
		//JLabel yearLabel = new JLabel("Confirm Password: ");
		JLabel yearLabel = new JLabel("     Year of Graduation: ");
		yearLabel.setFont(new Font("Garamond", Font.BOLD, 14));
	
		registerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String result = UserController.registerCheck(firstNameTextField.getText(), lastNameTextField.getText(), 
						emailTextField.getText(), idTextField.getText(), new String(passwordTextField.getPassword()), 
						new String(confirmPasswordTextField.getPassword()), majorTextField.getText(), Integer.parseInt((String) yearBox.getItemAt(yearBox.getSelectedIndex())),
						phoneTextField.getText());
				if (result.equals("SUCCESS")) {
					//registration successful, can go to the next page
					//now we need to verify text message
					displayPanel.swap(new TextVerificationPanel(displayPanel, userPanel, orgPanel));
					CurrentObjects.setAlmostRegisteredUser(UserController.newUser(firstNameTextField.getText(), lastNameTextField.getText(), 
						emailTextField.getText(), idTextField.getText(), new String (passwordTextField.getPassword()), 
						majorTextField.getText(), Integer.parseInt((String) yearBox.getItemAt(yearBox.getSelectedIndex()))));
				} else {
					JOptionPane.showMessageDialog(mainPanel, result, "Registration unsuccessful", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		

		
		mainPanel.add(firstnameLabel);
		mainPanel.add(firstNameTextField);
		mainPanel.add(lastnameLabel);
		mainPanel.add(lastNameTextField);
		mainPanel.add(emailLabel);
		mainPanel.add(emailTextField);
		mainPanel.add(idLabel);
		mainPanel.add(idTextField);
		mainPanel.add(majorLabel);
		mainPanel.add(majorTextField);
		mainPanel.add(yearLabel);
		mainPanel.add(yearBox);
		mainPanel.add(phoneLabel);
		mainPanel.add(phoneTextField);
		mainPanel.add(passwordLabel);
		mainPanel.add(passwordTextField);
		mainPanel.add(confirmPasswordLabel);
		mainPanel.add(confirmPasswordTextField);
		mainPanel.add(new JLabel(""));
		mainPanel.add(registerButton);
		
				
		

		return mainPanel;
	}
}
