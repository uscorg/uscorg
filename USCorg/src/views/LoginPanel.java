package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controllers.UserController;

public class LoginPanel extends JPanel {	
	public String userName;
	public String passWord;
	public JButton registerBut;
	public JButton loginButton;
	public JButton logoutButton;

	public JTextField loginText;
	//public JTextField passText;
	public JPasswordField passText;
	JLabel messageLab;
		//these textfield's will contain the user's text to pass into the backend
	JPanel displayPanel;
	JPanel userPanel;
	private DisplayPanel orgPanel;
	
	public LoginPanel( final DisplayPanel displayPanel, final DisplayPanel userPanel, final DisplayPanel orgPanel) {
		this.displayPanel = displayPanel;
		this.userPanel = userPanel;
		this.orgPanel = orgPanel;
		JLabel titleLab = new JLabel("  Please Login!");
		titleLab.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel loginLab = new JLabel("  Login (@usc.edu): ");
		loginLab.setFont(new Font("Garamond", Font.BOLD, 14));
		titleLab.setForeground(Color.LIGHT_GRAY);
		loginLab.setForeground(Color.LIGHT_GRAY);
		JLabel passLab = new JLabel("  Password: ");
		passLab.setFont(new Font("Garamond", Font.BOLD, 14));
		passLab.setForeground(Color.LIGHT_GRAY);
		JLabel blankLab1 = new JLabel("");
		messageLab = new JLabel("");
		messageLab.setForeground(Color.YELLOW);

		loginButton = new JButton("Login!");
		loginButton.setFont(new Font("Garamond", Font.BOLD, 14));
		loginButton.setForeground(Color.BLUE);

		JLabel registerLab = new JLabel("  Not a member?");
		registerLab.setFont(new Font("Garamond", Font.BOLD, 14));
		registerLab.setForeground(Color.RED);
		registerBut = new JButton("Click to Register");
		registerBut.setFont(new Font("Garamond", Font.BOLD, 14));
		//registerBut.addActionListener(new setDisplayPanelActionListener(displayPanel, new RegistrationPanel(displayPanel)));

		registerBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayPanel.swap(new RegistrationPanel(displayPanel, userPanel, orgPanel));
			}
			
		});
		registerBut.setForeground(Color.RED);
		loginText = new JTextField();
		loginText.setFont(new Font("Garamond", Font.BOLD, 14));
		passText = new JPasswordField();
		passText.setFont(new Font("Garamond", Font.BOLD, 14));
		passText.addActionListener(new ActionListener() {
			/*
			 * ActionListener for when Enter is pressed in the password field.
			 * Executes the same action as the loginButton's ActionListener.
			 */
			public void actionPerformed(ActionEvent e) {
				//technically should check if the button even has an action listener,
				//but we know it will have it, because we are adding it right below
				loginButton.getActionListeners()[0].actionPerformed(e);
			}
			
		});
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if (UserController.login(loginText.getText(), new String(passText.getPassword()))) {
					/*
					 * USER LOGGED IN
					 */
					messageLab.setText("  YOU LOGGED IN");
					messageLab.setFont(new Font("Garamond", Font.BOLD, 14));
					userPanel.swap(new LoggedinPanel(displayPanel, userPanel, orgPanel));
					orgPanel.swap(new CurrentOrgPanel(displayPanel, userPanel, orgPanel));
					displayPanel.swap(new WelcomePanel());
				} else {
					/*
					 * LOGIN FAILED
					 */
					messageLab.setText("  Incorrect Login.");
					messageLab.setFont(new Font("Garamond", Font.BOLD, 14));
				}
			}
		});
		super.setBackground(Color.DARK_GRAY);
		setLayout(new GridLayout(5,2));

		add(titleLab);
		add(blankLab1);
		add(loginLab);
		add(loginText);
		add(passLab);
		add(passText);
		add(messageLab);
		add(loginButton);
		add(registerLab);
		add(registerBut);
	}
}
