package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import threads.CommentsThread;
import threads.CommentsThreadEvent;
import controllers.AnnouncementController;
import controllers.EventController;
import controllers.OrgController;
import database.AnnouncementDB;
import database.EventDB;
import database.RSVP;
import database.UserDB;
import models.Announcement;
import models.CurrentObjects;
import models.Event;

public class EventPanel extends JPanel{
	DisplayPanel displayPanel;
	JButton deleteEventButton = new JButton ("Delete");
	JButton backButton = new JButton("Back");
	JLabel nameLabel = new JLabel("Event Name");
	JLabel timeLabel = new JLabel("Happening on");
	JButton addCommentButton;
	JButton addRSVPButton;
	JButton deleteRSVPButton;
	boolean ifRSVP = false;
	JLabel eventTitle = new JLabel();
	JLabel eventText = new JLabel();
	JTextArea[] eventPane;
	JScrollPane[] eventScrollPane;
	JPanel buttonsPanel;
	int orgID;
	JPanel commentPanel;
	JScrollPane commentScrollPane;
	
	CommentsThreadEvent commentsThread;
	
	private Event event;

	public EventPanel(int orgID, String name, DisplayPanel displayPanel)  {
		this.event = findEvent(orgID, name);
		this.displayPanel = displayPanel;
		this.orgID = orgID;
		

		
		JPanel mainPanel = new JPanel();
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.white);
		if (event == null) {
			add(new JLabel("This event does not exist. Good job, you broke the program."));
		} else {
			eventTitle.setText(event.getEventName());
			eventText.setText(event.getText());
			timeLabel.setText("Happening on: " + event.getMonth() + " " + event.getDay() + ", " + event.getYear());
			eventTitle.setFont(new Font("Garamond", Font.BOLD, 20));	
			eventText.setFont(new Font("Garamond", Font.ITALIC, 12));	
			
			add(getButtonsPanel());
			add(getEventPanel());
			add(getCommentPanel());
			add(new JLabel(""));
			
			commentsThread = new CommentsThreadEvent(event.getEventID(), commentScrollPane, commentPanel, displayPanel);
			CurrentObjects.getExecutor().submit(commentsThread);
		}
	}
	
	JPanel getButtonsPanel() {
		 buttonsPanel = new JPanel(new FlowLayout());
		backButton = new JButton("Back");
		backButton.setFont(new Font("Garamond",Font.BOLD,14));
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				displayPanel.swap(new OrganizationPanel(orgID, displayPanel));
			}
		});
		
		//checking user access here
		if (CurrentObjects.getUser().isLoggedIn()) {
			if (UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN") 
					|| UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("MEMBER")) {
				//admin + member are able to comment and RSVP for events
				addCommentButton = new JButton("Add Comment");
				addCommentButton.setFont(new Font("Garamond",Font.BOLD,14));
				addCommentButton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						displayPanel.swap(new NewCommentPanelEvent(event, displayPanel));
					}
				});
				
				addRSVPButton = new JButton("RSVP");
				deleteRSVPButton = new JButton("Delete RSVP");
				deleteRSVPButton.setEnabled(false);
				
				addRSVPButton.setFont(new Font("Garamond",Font.BOLD,14));
				addRSVPButton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
							JOptionPane.showMessageDialog(buttonsPanel,"You have RSVP'd for this event!");
							RSVP.insertRSVP(event.getEventID(), CurrentObjects.getUser().getUserID());
							deleteRSVPButton.setEnabled(true);
							addRSVPButton.setEnabled(false);

					}
				});
				deleteRSVPButton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
							JOptionPane.showMessageDialog(buttonsPanel,"You are no longer RSVP'd for this event!");
							RSVP.deleteRSVP(CurrentObjects.getUser().getUserID());
							//addRSVPButton.setText("RSVP");
							deleteRSVPButton.setEnabled(false);
							addRSVPButton.setEnabled(true);
					
					}
				});
				buttonsPanel.add(addRSVPButton);
				buttonsPanel.add(deleteRSVPButton);
				buttonsPanel.add(addCommentButton);
			}
		}
			if (UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN")) {
				//only admin should be able to delete
				deleteEventButton = new JButton ("Delete Event");
				deleteEventButton.setFont(new Font("Garamond",Font.BOLD,14));
				deleteEventButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(EventDB.deleteEvent(event.getEventID())) {
							JOptionPane.showMessageDialog(buttonsPanel,"Event Deleted!");
							displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
						}
						else {
							JOptionPane.showMessageDialog(buttonsPanel,"Error deleting event!\nPlease try again!", "Event Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				buttonsPanel.add(deleteEventButton);
			}

		
		buttonsPanel.add(backButton);
		//buttonsPanel.add(addCommentButton);
		//buttonsPanel.add(deleteEventButton);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getPreferredSize().height));
		return buttonsPanel;
	}
	
	JPanel getEventPanel() {
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		JLabel eventTitle = new JLabel();
		JTextPane eventText = new JTextPane();
		JLabel timeLabel = new JLabel();
		JScrollPane textScrollPane = new JScrollPane(eventText);
		
		eventTitle.setText(event.getEventName());
		eventTitle.setFont(new Font("Garamond", Font.BOLD, 32));	

		eventText.setText(event.getText());
		eventText.setFont(new Font("Garamond", Font.ITALIC, 18));
		eventText.setBorder(BorderFactory.createEmptyBorder(4, 0, 5, 0));
		eventText.setEditable(false);
		eventText.setFocusable(false);
		eventText.setOpaque(false);
		
		//textScrollPane.setPreferredSize(commentScrollPane.getPreferredSize());

		timeLabel.setText("Happening on: " + event.getMonthString() + " " + event.getDay() + ", " + event.getYear());
		timeLabel.setFont(new Font("Garamond", Font.PLAIN, 14));
		
		mainPanel.add(eventTitle);
		mainPanel.add(timeLabel);
		mainPanel.add(textScrollPane);
		
		mainPanel.setPreferredSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), mainPanel.getPreferredSize().height));
		return mainPanel;
	}
	
	JScrollPane getCommentPanel(){
		commentPanel = new JPanel();
		commentPanel.setBorder(BorderFactory.createTitledBorder("Comments"));
		commentScrollPane = new JScrollPane();

		commentScrollPane = new JScrollPane(commentPanel);
		commentScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		commentScrollPane.setMinimumSize(new Dimension(150,300));



		commentPanel.setLayout(new BoxLayout(commentPanel, BoxLayout.Y_AXIS));
		
		ArrayList<String> commentArray = new ArrayList<String>();
		ArrayList<String> commentTitleArray = new ArrayList<String>();

		for(int i = 0; i < 3; i++)
		{
			commentArray.add("Comment Text " + i);
			commentTitleArray.add("Comment Title " + i);
		}
		
		JLabel [] commentTitle = new JLabel[commentArray.size()];
		JLabel [] commentText = new JLabel[commentArray.size()];

		for(int i = 0; i < commentArray.size();i++)
		{
			commentTitle[i] = new JLabel();
			commentText[i] = new JLabel();
			commentTitle[i].setText(commentTitleArray.get(i));
			commentText[i].setText(commentArray.get(i));
			commentTitle[i].setFont(new Font("Garamond", Font.BOLD, 18));	
			commentText[i].setFont(new Font("Garamond", Font.ITALIC, 12));	

		}	
		for(int i = 0; i < commentTitle.length;i++)
		{
			commentPanel.add(commentTitle[i]);
			commentPanel.add(commentText[i]);

		}
		return commentScrollPane;
	}
	
	private Event findEvent(int orgID, String eventName) {
		Event result = null;
		Vector<Event> orgEvents = EventController.getEvent(orgID);
		for (int i = 0; i < orgEvents.size(); i++) {
			if (orgEvents.get(i).getEventName().equals(eventName)) {
				result = orgEvents.get(i);
				return result;
			}
		}
		return result;
	}
	

}
