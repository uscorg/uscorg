package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import models.CurrentObjects;
import models.Event;
import database.RSVP;

public class NotificationPanel extends JPanel {
	
	public DisplayPanel displayPanel;
	public static JTextArea event1Lab;
	public static JTextArea event2Lab;
	public static JTextArea event3Lab;
	public static JTextArea event4Lab;
	public static JTextArea event5Lab;
	

	
	public NotificationPanel(final DisplayPanel displayPanel) {
		this.displayPanel = displayPanel;

		
		JLabel titleLab = new JLabel("  Upcoming Events:                         ");
		titleLab.setFont(new Font("Garamond", Font.BOLD, 14));
		titleLab.setForeground(Color.DARK_GRAY);
		event1Lab = new JTextArea("  Event 1: ");
		event1Lab.setForeground(Color.DARK_GRAY);
		event1Lab.setFont(new Font("Garamond", Font.BOLD, 14));
		event1Lab.setLineWrap(true);
		event1Lab.setWrapStyleWord(true);
		event1Lab.setBackground(Color.LIGHT_GRAY);
		event1Lab.setEditable(false);


		event2Lab = new JTextArea("  Event 2: ");
		event2Lab.setForeground(Color.DARK_GRAY);
		event2Lab.setFont(new Font("Garamond", Font.BOLD, 14));
		event2Lab.setLineWrap(true);
		event2Lab.setWrapStyleWord(true);
		event2Lab.setEditable(false);

		event2Lab.setBackground(Color.LIGHT_GRAY);


		event3Lab = new JTextArea("  Event 3: ");
		event3Lab.setForeground(Color.DARK_GRAY);
		event3Lab.setFont(new Font("Garamond", Font.BOLD, 14));
		event3Lab.setLineWrap(true);
		event3Lab.setWrapStyleWord(true);
		event3Lab.setEditable(false);

		event3Lab.setBackground(Color.LIGHT_GRAY);

		event4Lab = new JTextArea("  Event 4: ");
		event4Lab.setForeground(Color.DARK_GRAY);
		event4Lab.setFont(new Font("Garamond", Font.BOLD, 14));
		event4Lab.setLineWrap(true);
		event4Lab.setWrapStyleWord(true);
		event4Lab.setEditable(false);
		event4Lab.setBackground(Color.LIGHT_GRAY);

		event5Lab = new JTextArea("  Event 5: ");
		event5Lab.setForeground(Color.DARK_GRAY);
		event5Lab.setFont(new Font("Garamond", Font.BOLD, 14));
		event5Lab.setLineWrap(true);
		event5Lab.setEditable(false);

		event5Lab.setBackground(Color.LIGHT_GRAY);

		//event5Lab.setWrapStyleWord(true);

		setBackground(Color.LIGHT_GRAY);
		setLayout(new GridLayout(6,1));
		add(titleLab);
		add(event1Lab);

		add(event2Lab);

		add(event3Lab);
		
		add(event4Lab);
		
		add(event5Lab);
		


		
		
		// TODO Auto-generated constructor stub
	}
	
	public static void changeLabels(){
		Vector <Event> events = RSVP.fetchMostRecentRSVP(CurrentObjects.getUser().getUserID());
		if (events.size()>=1){
			event1Lab.setText("  Event 1: " + events.elementAt(0).toString());
		}
		if (events.size()>=2){
			event2Lab.setText("  Event 2: " + events.elementAt(1).toString());
		}
		if (events.size()>=3){
			event3Lab.setText("  Event 3: " + events.elementAt(2).toString());
		}
		if (events.size()>=4){
			event4Lab.setText("  Event 4: " + events.elementAt(3).toString());
		}
		if (events.size()==5){
			event5Lab.setText("  Event 5: " + events.elementAt(4).toString());
		}
	}
	
	public static void clearLabels(){
		event1Lab.setText("  Event 1: ");
		event2Lab.setText("  Event 2: ");
		event3Lab.setText("  Event 3: ");
		event4Lab.setText("  Event 4: ");
		event5Lab.setText("  Event 5: ");
	}

}
