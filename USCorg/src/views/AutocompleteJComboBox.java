package views;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/*
 * This is a customized JComboBox with an autocomplete function.
 * As the user enters input, suggestions will show up in a
 * dropdown list, and they will change depending on the
 * input.
 * Will be used for tagging and similar functions.
 */


public class AutocompleteJComboBox extends JComboBox {
	List<String> listToSearch;
	Component component = getEditor().getEditorComponent();
	JTextComponent textComponent = (JTextComponent) component;

	public AutocompleteJComboBox(final List<String> listToSearch) {
		super();
		setEditable(true);
		
		this.listToSearch = listToSearch;

		if (component instanceof JTextComponent) {
			textComponent.getDocument().addDocumentListener(new DocumentListener() {
				/*
				 * this is where the search for the dropdown list will happen
				 */

				@Override
				public void changedUpdate(DocumentEvent arg0) {
				}

				@Override
				public void insertUpdate(DocumentEvent arg0) {
					update();
				}

				@Override
				public void removeUpdate(DocumentEvent arg0) {
					update();
				}
				
				public void update() {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							List<String> founds = new ArrayList<String>(search(listToSearch, textComponent.getText()));
							Set<String> foundSet = new HashSet<String>();
							for (String string : founds) {
								//making a set of found values
								foundSet.add(string.toLowerCase());
							}
							Collections.sort(founds);
							
							setEditable(false);
							removeAllItems();
							
							if(!foundSet.contains(textComponent.getText().toLowerCase())) {
								//no current value in the founds, add it
								addItem(textComponent.getText());
							}
							
							for (String string : founds) {
								//add the rest to the dropdown
								addItem(string);
							}
							setEditable(true);
							setPopupVisible(true);
							textComponent.grabFocus();
							textComponent.requestFocus();
						}
						
					});
				}
				
			});
			textComponent.addFocusListener(new FocusListener() {
				/* 
				 * Every time a user clicks on the list, a dropdown
				 * with the list will appear
				 */
				
				@Override
				public void focusGained(FocusEvent arg0) {
					if (textComponent.getText().length() > 0) {
						setPopupVisible(true);

					}

				}

				@Override
				public void focusLost(FocusEvent arg0) {	
					
				}
				
			});
		} else {
			throw new IllegalStateException("The edit component is not a JTextComponent.");
		}
	}
	
	private List<String> search (List<String> listToSearch, String value) {
		//the search function, returns a list of strings whose first
		//values match the values of the string entered
		
		List<String> foundList = new ArrayList<String>();
		
		for (String string : listToSearch) {
			if (string.indexOf(value) == 0) {
				foundList.add(string);
			}
		}
		
		return foundList;
	}
	
}
