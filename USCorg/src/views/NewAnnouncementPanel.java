package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import views.NewEventPanel.Months;
import models.CurrentObjects;
import controllers.AnnouncementController;
import controllers.EventController;
import controllers.UserController;

public class NewAnnouncementPanel extends JPanel{
	JButton createButton = new JButton ("Create Announcement");

	JLabel panelLabel = new JLabel("New Announcement");
	JButton cancelButton = new JButton ("Cancel");

	JTextField titleTextField = new JTextField();
	JTextArea textTextField = new JTextArea();
	

	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	
	public NewAnnouncementPanel( DisplayPanel displayPanel)  {
		panelLabel.setFont(new Font("Garamond",Font.BOLD,14));
		createButton.setFont(new Font("Garamond",Font.BOLD,14));
		cancelButton.setFont(new Font("Garamond",Font.BOLD,14));
		titleTextField.setFont(new Font("Garamond",Font.BOLD,14));
		textTextField.setFont(new Font("Garamond",Font.BOLD,14));
		this.displayPanel = displayPanel;
		setLayout(new BorderLayout());
		setBackground(Color.white);

		//JLabel panelLabel = new JLabel("                                                                                New Announcement");

		add(panelLabel, BorderLayout.NORTH);
		
		add(getNewAnnouncementPanel(), BorderLayout.CENTER);
	}
	
	JPanel getButtonsPanel() {
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getHeight()));
		
		createButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				String result = AnnouncementController.newAnnouncement(CurrentObjects.getOrg().getOrgID(), CurrentObjects.getUser().getUserID(), titleTextField.getText(), textTextField.getText());
				if (result.equals("Success!")) {
					JOptionPane.showMessageDialog(mainPanel,"Announcement Created!");
					displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
				}
				else if (result.equals("Announcement text must be less than 550 characters!")){
					JOptionPane.showMessageDialog(mainPanel,result, "Announcement Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					JOptionPane.showMessageDialog(mainPanel,"Error creating announcement!\nPlease try again!", "Announcement Error", JOptionPane.ERROR_MESSAGE);
				}
			
			}
			
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
			}
		});
		
		buttonsPanel.add(createButton);
		buttonsPanel.add(cancelButton);
		return buttonsPanel;		
	}
	
	JPanel getNewAnnouncementPanel() {
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.white);
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridBagLayout());
		innerPanel.setBackground(Color.white);
		
		GridBagConstraints c = new GridBagConstraints();
		
		textTextField.setEditable(true);	
		textTextField.setLineWrap(true);
		textTextField.setWrapStyleWord(true);
		JScrollPane textScroll = new JScrollPane(textTextField);
		
		JLabel titleLabel = new JLabel("Announcement Title: ");
		titleLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel textLabel = new JLabel("Announcement Text: ");
		textLabel.setFont(new Font("Garamond",Font.BOLD,14));

		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(Color.white);
		titleTextField.setPreferredSize(new Dimension(600, 30));
		titlePanel.add(titleLabel);
		titlePanel.add(titleTextField);

		
		JPanel textPanel = new JPanel();
		textPanel.setBackground(Color.white);
		//textTextField.setPreferredSize(new Dimension(200, 30));
		textPanel.add(textLabel);
		textPanel.add(textScroll);
		textScroll.setPreferredSize(new Dimension(600, 200));
		
		JPanel boxPanel = new JPanel();
		boxPanel.setBackground(Color.white);
		boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
		boxPanel.add(panelLabel);
		boxPanel.add(titlePanel);
		boxPanel.add(textPanel);
		boxPanel.add(getButtonsPanel());
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_START;
		c.weighty = 1.0;
		innerPanel.add(boxPanel, c);

		mainPanel.add(innerPanel);

		return mainPanel;
	}
}
