package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import models.CurrentObjects;
import controllers.OrgController;
import controllers.UserController;

public class EditOrganizationPanel extends JPanel {
	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	
	JButton editButton = new JButton ("Edit Organization");
	JButton cancelButton = new JButton ("Cancel");
	JTextField meetingTextField = new JTextField(CurrentObjects.getOrg().getMT());
	JTextField contactTextField = new JTextField(CurrentObjects.getOrg().getContact());
	JTextField numberMembersTextField = new JTextField(Integer.toString(CurrentObjects.getOrg().getMemberNo()));
	JTextField duesTextField = new JTextField(Integer.toString(CurrentObjects.getOrg().getDues()));
	String[] audienceArray = {"Undergrad", "Grad", "Both"};
	JComboBox<String> audienceCombo = new JComboBox<String>(audienceArray);	JTextArea descriptionTextArea = new JTextArea(10,20);

	public EditOrganizationPanel(DisplayPanel displayPanel)  {
		editButton.setFont(new Font("Garamond",Font.BOLD,14));
		cancelButton.setFont(new Font("Garamond",Font.BOLD,14));
		meetingTextField.setFont(new Font("Garamond",Font.BOLD,14));
		contactTextField.setFont(new Font("Garamond",Font.BOLD,14));
		numberMembersTextField.setFont(new Font("Garamond",Font.BOLD,14));
		duesTextField.setFont(new Font("Garamond",Font.BOLD,14));
		audienceCombo.setFont(new Font("Garamond",Font.BOLD,14));
		audienceCombo.setSelectedItem(CurrentObjects.getOrg().getAudience());
		descriptionTextArea.setFont(new Font("Garamond",Font.BOLD,14));
		this.displayPanel = displayPanel;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.white);
		descriptionTextArea.setText(CurrentObjects.getOrg().getDes());
		descriptionTextArea.setEditable(true);	
		descriptionTextArea.setLineWrap(true);
		descriptionTextArea.setWrapStyleWord(true);
		JLabel descriptionLabel = new JLabel("Description");
		descriptionLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JScrollPane descriptionScroll = new JScrollPane(descriptionTextArea);
		add(descriptionLabel);
		add(descriptionScroll);
		add(getEditOrganizationPanel());
		add(getButtonsPanel());
	}
	
	JPanel getButtonsPanel() {
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getHeight()));
		
		editButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e)
            {
	        	if (OrgController.updateOrganization(CurrentObjects.getOrg().getName(), descriptionTextArea.getText(), meetingTextField.getText(), contactTextField.getText(), numberMembersTextField.getText(), duesTextField.getText(), (String) audienceCombo.getSelectedItem(), CurrentObjects.getOrg().getOrgID() )) {
	        		JOptionPane.showMessageDialog(mainPanel, "Edit Successful", "Update Successful!", JOptionPane.INFORMATION_MESSAGE);
	        		displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
	        	} else {
	        		JOptionPane.showMessageDialog(mainPanel, "Edit Failed", "Update Unsuccessful!", JOptionPane.WARNING_MESSAGE);
	        	}
        	}
        }); 
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
			}
		});
		
		buttonsPanel.add(editButton);
		buttonsPanel.add(cancelButton);
		return buttonsPanel;
	}
	
	JPanel getEditOrganizationPanel() {
		mainPanel.setLayout(new GridLayout(6,2));
		mainPanel.setBackground(Color.white);
		
		JLabel meetingLabel = new JLabel("Meeting Times: ");
		meetingLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel contactLabel = new JLabel("Contact: ");
		contactLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel numberMembersLabel = new JLabel("Number of Members: ");
		numberMembersLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel duesLabel = new JLabel("Dues: ");
		duesLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel audienceLabel = new JLabel("Type of Audience: ");
		audienceLabel.setFont(new Font("Garamond",Font.BOLD,14));
		
		mainPanel.add(meetingLabel);
		mainPanel.add(meetingTextField);
		mainPanel.add(contactLabel);
		mainPanel.add(contactTextField);
		mainPanel.add(numberMembersLabel);
		mainPanel.add(numberMembersTextField);
		mainPanel.add(duesLabel);
		mainPanel.add(duesTextField);
		mainPanel.add(audienceLabel);
		mainPanel.add(audienceCombo);
			
		return mainPanel;
	}
}