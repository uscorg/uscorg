package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import models.Announcement;
import models.CurrentObjects;
import controllers.AnnouncementController;
import controllers.CommentController;
import controllers.UserController;

public class NewCommentPanel extends JPanel{
	JButton createButton = new JButton ("Create Comment");
	

	JLabel panelLabel = new JLabel("New Comment");
	JButton cancelButton = new JButton ("Cancel");

	JTextArea textTextField = new JTextArea();
	

	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	
	private Announcement announcement;
	
	public NewCommentPanel(Announcement announcement, DisplayPanel displayPanel)  {
		this.announcement = announcement;
		
		createButton.setFont(new Font("Garamond",Font.BOLD,14));
		panelLabel.setFont(new Font("Garamond",Font.BOLD,14));
		cancelButton.setFont(new Font("Garamond",Font.BOLD,14));
		textTextField.setFont(new Font("Garamond",Font.BOLD,14));
		this.displayPanel = displayPanel;
		setLayout(new BorderLayout());
		setBackground(Color.white);

		add(getNewCommentPanel(), BorderLayout.CENTER);
	}
	
	JPanel getButtonsPanel() {
		JPanel buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getHeight()));
		
		createButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (CommentController.newComment(textTextField.getText(), announcement.getAnnouncementID(), CurrentObjects.getUser().getUserID())) {
					displayPanel.swap(new AnnouncementPanel(announcement.getOrg(), announcement.getTitle(), displayPanel));
				}
				else {
					JOptionPane.showMessageDialog(mainPanel,"Error creating comment!\nPlease try again!", "Comment Error", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
			}
		});
		
		buttonsPanel.add(createButton);
		buttonsPanel.add(cancelButton);
		
		return buttonsPanel;		
	}
	
	JPanel getNewCommentPanel() {
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.white);
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridBagLayout());
		innerPanel.setBackground(Color.white);
		
		GridBagConstraints c = new GridBagConstraints();
		
		textTextField.setEditable(true);	
		textTextField.setLineWrap(true);
		textTextField.setWrapStyleWord(true);
		JScrollPane textScroll = new JScrollPane(textTextField);
		
		JLabel textLabel = new JLabel("Comment Text: ");
		textLabel.setFont(new Font("Garamond",Font.BOLD,14));

		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(Color.white);

		
		JPanel textPanel = new JPanel();
		textPanel.setBackground(Color.white);
		//textTextField.setPreferredSize(new Dimension(200, 30));
		textPanel.add(textLabel);
		textPanel.add(textScroll);
		textScroll.setPreferredSize(new Dimension(600, 200));
		
		JPanel boxPanel = new JPanel();
		boxPanel.setBackground(Color.white);
		boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
		boxPanel.add(panelLabel);
		boxPanel.add(textPanel);
		boxPanel.add(getButtonsPanel());
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_START;
		c.weighty = 1.0;
		innerPanel.add(boxPanel, c);

		mainPanel.add(innerPanel);

		return mainPanel;
	}
}