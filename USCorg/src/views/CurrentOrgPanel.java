package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import models.CurrentObjects;
import models.Org;
import database.OrgDB;
import database.UserDB;

public class CurrentOrgPanel extends JPanel implements TreeSelectionListener {
	
	private static JTree organizationTree;
	private static ArrayList<String> currentOrgArray;
	private JLabel categoryLabel;
	private DisplayPanel displayPanel;
	private DisplayPanel orgPanel;
	JButton newOrganizationButton;
		//We need to meet in person in order to sort this shit out but I basically made an arraylist
	//of categories
	//I'm assuming we'll then use this to add into each thing as I did underneath manually -Priyam
	public CurrentOrgPanel(final DisplayPanel displayPanel, final DisplayPanel userPanel, final DisplayPanel orgPanel)
	{
		if (CurrentObjects.getUser().isLoggedIn()) {
			this.displayPanel = displayPanel;
			this.orgPanel = orgPanel;
			this.setLayout(new GridLayout(2,1));
			//this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			currentOrgArray = new ArrayList<String>();

			Vector <Org> temp = UserDB.fetchUserOrganizations(CurrentObjects.getUser().getUserID());
			DefaultMutableTreeNode root = new DefaultMutableTreeNode(" Current Organizations");
			for(int i = 0; i < temp.size(); i++)
			{
				root.add(new DefaultMutableTreeNode(temp.elementAt(i).getName()));
			}
			
			
			setBackground(Color.WHITE);
			organizationTree = new JTree(root);
			organizationTree.setBackground(Color.WHITE);
			organizationTree.setFont(new Font("Garamond", Font.BOLD, 14));
			organizationTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
			organizationTree.addTreeSelectionListener(this);
			
			//organizationTree.
			add(organizationTree);
			newOrganizationButton = new JButton("Create Organization");
			newOrganizationButton.setFont(new Font("Garamond",Font.BOLD,14));
				
			newOrganizationButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					displayPanel.swap(new NewOrganizationPanel(displayPanel, userPanel, orgPanel));
				}
			});
			add(newOrganizationButton);
		}

	}
	public static void changeUserTree(){
		Vector <Org> temp= new Vector<Org>();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(" Current Organizations");
		for(int i = 0; i < currentOrgArray.size(); i++)
		{
			DefaultMutableTreeNode categoryNode = new DefaultMutableTreeNode(currentOrgArray.get(i));
			temp = UserDB.fetchUserOrganizations(CurrentObjects.getUser().getUserID());
			for (int j = 0; j < temp.size(); ++j){
				categoryNode.add(new DefaultMutableTreeNode(temp.elementAt(j).getName()));
			}
			root.add(categoryNode);
		}
		organizationTree = new JTree(root);
		organizationTree.setBackground(Color.WHITE);
		organizationTree.setFont(new Font("Garamond", Font.BOLD, 14));
		organizationTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		//organizationTree.addTreeSelectionListener();
		organizationTree.revalidate();
		organizationTree.repaint();
	}
	public void valueChanged(TreeSelectionEvent arg0) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) organizationTree.getLastSelectedPathComponent();
		if(node == null)
			return;
		Object nodeInfo = node.getUserObject();
		
		if(node.isLeaf()) {
			String temp = nodeInfo.toString();
			JScrollPane orgScroll = new JScrollPane();
			Org tempOrg = OrgDB.fetchOrgwithOrgname(temp);
			orgScroll = new JScrollPane(new OrganizationPanel(tempOrg.getOrgID(), displayPanel));
			displayPanel.swap(orgScroll);
		} 
}
}