package views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import models.Org;
import controllers.OrgController;
import utils.Pair;

public class SearchResultsPanel extends JPanel {
	
	private JLabel [] announcementTitle;
	private JLabel [] announcementText;
	private JPanel announcementsPanel;
	DisplayPanel displayPanel;
	Vector< Org > searchResults;
	
	
	public SearchResultsPanel(String keyword, final DisplayPanel displayPanel) {
		this.displayPanel = displayPanel;
		searchResults = OrgController.searchOrgs(keyword);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel resultsDisplayPanel = new JPanel();
		if(searchResults.size() < 10)
		{
			resultsDisplayPanel.setLayout(new GridLayout(10,1));

		}
		else
		{
			resultsDisplayPanel.setLayout(new GridLayout(searchResults.size(),1));

		}
		
		JLabel titleLabel = new JLabel("Your search results are below. You searched for: " + keyword);
		titleLabel.setFont(new Font("Garamond", Font.BOLD, 20));
		JLabel resultsLabel;
		
		if (searchResults.isEmpty()) {
			//nothing was found
			resultsLabel = new JLabel("Sorry, nothing was found! Maybe try a different keyword?");
			resultsLabel.setFont(new Font("Garamond", Font.ITALIC, 16));
			resultsDisplayPanel.add(resultsLabel);
		} else {
			//something was found
			resultsLabel = new JLabel("USC.org found " + searchResults.size() + " organizations that match your search.");
			final JPanel [] announcementsPanels = new JPanel[searchResults.size()];
			JLabel [] announcementTitle = new JLabel[searchResults.size()];
			JLabel [] announcementText = new JLabel[searchResults.size()];
			for (int i = 0; i < searchResults.size(); i++) {
				announcementsPanels[i] = new JPanel();
				//announcementsPanels[i].setLayout(new BoxLayout(announcementsPanels[i],BoxLayout.Y_AXIS));
				announcementsPanels[i].setLayout(new BorderLayout());
				announcementsPanels[i].setBorder(BorderFactory.createRaisedBevelBorder());
				announcementTitle[i] = new JLabel(searchResults.get(i).getName());				
				announcementTitle[i].setFont(new Font("Garamond", Font.BOLD, 18));
				announcementText[i] = new JLabel(searchResults.get(i).getDes());
				announcementText[i].setFont(new Font("Garamond", Font.ITALIC, 12));
				int orgID = searchResults.get(i).getOrgID();
				int a = i;
				announcementsPanels[i].addMouseListener(new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
					}

					public void mouseEntered(MouseEvent e) {
					}

					public void mouseExited(MouseEvent e) {
					}
					public void mousePressed(MouseEvent e) {
						//simulate a button look
						announcementsPanels[a].setBorder(BorderFactory.createLoweredBevelBorder());
					}
					public void mouseReleased(MouseEvent e) {
						//change the screen + signal the thread to stop
						announcementsPanels[a].setBorder(BorderFactory.createRaisedBevelBorder());
						displayPanel.swap(new OrganizationPanel(orgID, displayPanel));
					}
				});
				
				announcementsPanels[i].add(announcementTitle[i], BorderLayout.NORTH);
				announcementsPanels[i].add(announcementText[i], BorderLayout.CENTER);
				resultsDisplayPanel.add(announcementsPanels[i]);
				resultsDisplayPanel.setPreferredSize(new Dimension(150,150));

			}
		}
		
		add(titleLabel, BorderLayout.NORTH);
		add(resultsDisplayPanel, BorderLayout.CENTER);
	}
	
	
}
