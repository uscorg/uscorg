package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.CurrentObjects;
import controllers.UserController;

public class TextVerificationPanel extends JPanel {
	final DisplayPanel displayPanel;
	final DisplayPanel userPanel;
	JPanel verificationPanel = new JPanel();
	DisplayPanel orgPanel;

	
	public TextVerificationPanel(final DisplayPanel displayPanel, final DisplayPanel userPanel, final DisplayPanel orgPanel) {
		this.displayPanel = displayPanel;
		this.userPanel = userPanel;
		this.orgPanel = orgPanel;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel titleLabel = new JLabel("Phone Verification");
		titleLabel.setFont(new Font("Garamond", Font.BOLD, 30));
		JLabel descriptionLabel = new JLabel("We just sent a text to your phone! Please enter the verification code below to confirm your phone number.");
		descriptionLabel.setFont(new Font("Garamond", Font.ITALIC, 16));
		descriptionLabel.setForeground(Color.BLUE);
		
		verificationPanel.setLayout(new GridLayout(16,2));
		JLabel verificationLabel = new JLabel("Verify code: ");
		verificationLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		final JTextField verificationTextField = new JTextField();
		verificationTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		verificationTextField.setHorizontalAlignment(JTextField.CENTER);
		JButton submitButton = new JButton("Submit");
		submitButton.setFont(new Font("Garamond", Font.BOLD, 14));
		verificationPanel.add(verificationLabel);
		verificationPanel.add(verificationTextField);
		verificationPanel.add(submitButton);
		
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (UserController.checkVerification(verificationTextField.getText())) {
					//verification successful
					UserController.registerUser(CurrentObjects.getAlmostRegisteredUser().getFirstName(), CurrentObjects.getAlmostRegisteredUser().getLastName(), 
							CurrentObjects.getAlmostRegisteredUser().getUscName(), CurrentObjects.getAlmostRegisteredUser().getStudentID(), 
							CurrentObjects.getAlmostRegisteredUser().getPassword(), CurrentObjects.getAlmostRegisteredUser().getMajor(),
							CurrentObjects.getAlmostRegisteredUser().getYearOf());
					displayPanel.swap(new WelcomePanel());
					userPanel.swap(new LoggedinPanel(displayPanel, userPanel, orgPanel));
					orgPanel.swap(new CurrentOrgPanel(displayPanel, userPanel, orgPanel));
					orgPanel.revalidate();
					orgPanel.repaint();
				} else {
					//code not verified
					JOptionPane.showMessageDialog(verificationPanel, "The verification code does not match, try again!", "Wrong Code", JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		add(titleLabel);
		add(descriptionLabel);
		add(verificationPanel);
		
	}
}
