package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import database.UserDB;
import controllers.OrgController;
import controllers.UserController;
import models.CurrentObjects;
import threads.AnnouncementsThread;
import threads.EventsThread;

/* Registration panel that will go in place of the central
 * display panel in the GUI. Shows up when a user clicks
 * the "Click to Register" button.
 */

public class OrganizationPanel extends JPanel {	
	JLabel nameLabel = new JLabel("Organization Name");
	private ArrayList<String> announcementArray;	
	private ArrayList<String> announcementTitleArray;	
	DisplayPanel displayPanel;
	private AnnouncementsThread announcementsThread;

	JLabel [] announcementTitle;
	JLabel[] announcementText;
	JPanel announcementsPanel;
	JScrollPane announcementScrollPane;
	
	private ArrayList<String> eventArray;	
	private ArrayList<String> eventTitleArray;	
	
	private EventsThread eventsThread;


	JLabel [] eventTitle;
	JLabel[] eventText;
	JPanel eventsPanel;
	JScrollPane eventScrollPane;
	
	
	
	public OrganizationPanel(int orgID, DisplayPanel displayPanel)  {
		CurrentObjects.setOrg(OrgController.getOrgWithID(orgID));
		this.nameLabel.setText(CurrentObjects.getOrg().getName());
		this.displayPanel = displayPanel;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(Color.WHITE);
		add(new ImagePanel());
		add(getOrganizationPanel());
		
		announcementsThread = new AnnouncementsThread(announcementTitle, announcementText, announcementsPanel, announcementScrollPane, CurrentObjects.getOrg().getOrgID(), displayPanel);
		CurrentObjects.getExecutor().submit(announcementsThread);
		eventsThread = new EventsThread(eventTitle, eventText, eventsPanel, eventScrollPane, CurrentObjects.getOrg().getOrgID(), displayPanel);
		CurrentObjects.getExecutor().submit(eventsThread);
	}
	
	JPanel getOrganizationPanel() {

		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		mainPanel.setPreferredSize(new Dimension(800, 600));
		GridBagConstraints c = new GridBagConstraints();

		Font font = nameLabel.getFont();
		nameLabel.setFont(font.deriveFont(Font.BOLD));
		nameLabel.setForeground(Color.BLUE);
		nameLabel.setFont(new Font("Garamond",Font.BOLD,40));
		
		JButton editOrgButton = new JButton("Edit Org");
		editOrgButton.setFont(new Font("Garamond",Font.BOLD,14));
		editOrgButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new EditOrganizationPanel(displayPanel));
			}
		});
		JButton addAnnouncementButton = new JButton("+A");
		addAnnouncementButton.setFont(new Font("Garamond",Font.BOLD,14));
		addAnnouncementButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new NewAnnouncementPanel(displayPanel));
			}
		});
		JButton addEventButton = new JButton("+E");
		addEventButton.setFont(new Font("Garamond",Font.BOLD,14));
		addEventButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new NewEventPanel(displayPanel));
			}
		});

		c.gridx = 1;
		c.gridy = 0;
		//c.weightx = 0.5;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		mainPanel.add(nameLabel, c);
		
		JPanel adminPanel = new JPanel();
		//adminPanel.setLayout(new FlowLayout());
		adminPanel.setLayout(new BoxLayout(adminPanel,BoxLayout.Y_AXIS));
		editOrgButton.setPreferredSize(new Dimension(90, 30));
		addAnnouncementButton.setPreferredSize(new Dimension(50, 30));
		addEventButton.setPreferredSize(new Dimension(50, 30));
		
		adminPanel.add(addEventButton);
		adminPanel.add(addAnnouncementButton);
		adminPanel.add(editOrgButton);
		if (UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN")) {
			c.gridx = 2;
			c.gridy = 0;
			//c.weightx = 0.2;
			//c.gridwidth = 1;
			c.fill = GridBagConstraints.NONE;
			c.anchor = GridBagConstraints.FIRST_LINE_END;
			mainPanel.add(adminPanel, c);
		}
		if (!UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN") && 
				!UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("MEMBER") &&
				CurrentObjects.getUser().isLoggedIn()) {
			JButton joinOrgButton = new JButton("Join Org!");
			joinOrgButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					UserController.addPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID(), "MEMBER");
					displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
				}
			});
			c.gridx = 2;
			c.gridy = 0;
			c.fill = GridBagConstraints.NONE;
			c.anchor = GridBagConstraints.FIRST_LINE_END;
			mainPanel.add(joinOrgButton);
		}
		
		
		JPanel leftPanel = new JPanel();
		
		
		
		
		//leftPanel.setPreferredSize(new Dimension(200,50));
		leftPanel.setBorder(BorderFactory.createTitledBorder("Description"));
		JTextArea description = new JTextArea(5,20);
		JScrollPane descriptionScroll = new JScrollPane(description);
		description.setText(CurrentObjects.getOrg().getDes());
		description.setFont(new Font("Garamond",Font.BOLD,14));
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setOpaque(false);
		description.setFocusable(false);
		description.setEditable(false);
		
		leftPanel.add(descriptionScroll);
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 0.3;
		c.weighty = 1.0;
		//c.insets = new Insets(0,0,150, 0);
		c.gridwidth = 1;
		c.gridheight = 2;
		c.fill = GridBagConstraints.VERTICAL;
		c.anchor = GridBagConstraints.LINE_START;
		mainPanel.add(leftPanel, c);
		
		
		JPanel rightPanel = new JPanel();
		rightPanel.setBorder(BorderFactory.createTitledBorder("Organization Stats"));
		rightPanel.setPreferredSize(new Dimension(150, 50));
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		JLabel meetingTimesLabel = new JLabel("Meeting Times: " + CurrentObjects.getOrg().getMT());
		JLabel contactLabel = new JLabel("Contact: " + CurrentObjects.getOrg().getContact());
		JLabel numberMembersLabel = new JLabel("Number of Members: " + CurrentObjects.getOrg().getMemberNo());
		JLabel duesLabel = new JLabel("Dues: " + CurrentObjects.getOrg().getDues());
		JLabel audienceLabel = new JLabel("Audience: " + CurrentObjects.getOrg().getAudience());
		meetingTimesLabel.setFont(new Font("Garamond",Font.BOLD,14));
		contactLabel.setFont(new Font("Garamond",Font.BOLD,14));
		numberMembersLabel.setFont(new Font("Garamond",Font.BOLD,14));
		duesLabel.setFont(new Font("Garamond",Font.BOLD,14));
		audienceLabel.setFont(new Font("Garamond",Font.BOLD,14));
		
		JPanel [] rowPanels = new JPanel[6];
		for (int i = 0; i < 6; i++) {
			rowPanels[i] = new JPanel();
		}
		rowPanels[0].add(meetingTimesLabel);
		rowPanels[1].add(contactLabel);
		rowPanels[2].add(numberMembersLabel);
		rowPanels[3].add(duesLabel);
		rowPanels[4].add(audienceLabel);
		
		for (int i = 0; i < 6; i++) {
			rightPanel.add(rowPanels[i]);
		}
		c.gridx = 2;
		c.gridy = 1;
		c.weightx = 0.3;
		//c.weighty = 1.0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.insets = new Insets(0,0,350, 0);
		c.fill = GridBagConstraints.VERTICAL;
		c.anchor = GridBagConstraints.LINE_END;
		
		//mainPanel.add(rightPanel, c);
		leftPanel.add(rightPanel);
		
		//ANNOUNCEMENTS
		announcementsPanel = new JPanel();
		
		announcementScrollPane = new JScrollPane(announcementsPanel);
		announcementScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		announcementScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		announcementScrollPane.setPreferredSize(new Dimension(150,150));

		announcementsPanel.setLayout(new BoxLayout(announcementsPanel, BoxLayout.Y_AXIS));
		announcementsPanel.setBorder(BorderFactory.createTitledBorder("Announcements"));
		announcementsPanel.add(new JLabel("Loading..."));
		
		
		//EVENTS
		eventsPanel = new JPanel();
		
		eventScrollPane = new JScrollPane(eventsPanel);
		eventScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		eventScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		eventScrollPane.setPreferredSize(new Dimension(150,150));

		eventsPanel.setLayout(new BoxLayout(eventsPanel, BoxLayout.Y_AXIS));
		eventsPanel.setBorder(BorderFactory.createTitledBorder("Events"));
		eventsPanel.add(new JLabel("Loading..."));
		
		//END
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		centerPanel.add(announcementScrollPane);
		centerPanel.add(eventScrollPane);
		c.gridx = 1;
		c.gridy = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.ipady = 0;
		c.ipadx = 50;
		c.insets = new Insets(0,0,0, 0);
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		mainPanel.add(centerPanel, c);

			//add announcements button
			//make announcement panel they should have next previous button (show 5 announcements at a time, make it scrollable
			//make checklists and combo boxes for everuthing else
		//categories/interests 
		
		
		
		return mainPanel;
	}
	
	public void loadAnnouncements() {
		
	}
}