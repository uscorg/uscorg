package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controllers.UserController;
import database.RSVP;
import models.CurrentObjects;

public class LoggedinPanel extends JPanel {
	JPanel displayPanel;
	JPanel userPanel;
	DisplayPanel orgPanel;
	String username;
	
	public LoggedinPanel(final DisplayPanel displayPanel, final DisplayPanel userPanel, final DisplayPanel orgPanel) {
		this.displayPanel = displayPanel;
		this.orgPanel = orgPanel;
		username = CurrentObjects.getUser().getFirstName();
		JLabel titleLab = new JLabel("  Welcome to USC.org, " + username + "!");
		titleLab.setFont(new Font("Garamond", Font.BOLD, 14));
		titleLab.setForeground(Color.LIGHT_GRAY);
		JLabel blankLab1 = new JLabel("");

		JButton logoutButton = new JButton("Log out");
		logoutButton.setFont(new Font("Garamond", Font.BOLD, 14));
		logoutButton.setForeground(Color.BLUE);
		logoutButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * Clean up of the displayPanel to restore to the logged out default
				 */
				JOptionPane.showMessageDialog(null, "Logging Out", "USC.org Message", JOptionPane.INFORMATION_MESSAGE);
				userPanel.swap(new LoginPanel(displayPanel, userPanel, orgPanel));
				displayPanel.swap(new WelcomePanel());
				CurrentObjects.setUser(UserController.makeGuestUser());
				orgPanel.swap(new CurrentOrgPanel(displayPanel, userPanel, orgPanel));
				orgPanel.revalidate();
				orgPanel.repaint();
			}
		});
		JButton editAccountButton = new JButton("Edit Account");
		editAccountButton.setFont(new Font("Garamond", Font.BOLD, 14));
		editAccountButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new AccountPanel(displayPanel,userPanel));
			}
		});
		
		setBackground(Color.DARK_GRAY);
		setLayout(new GridLayout(4,2));

		add(titleLab);
		add(blankLab1);
		add(logoutButton);
		add(editAccountButton);
	}
}
