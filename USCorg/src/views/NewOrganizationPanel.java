package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import views.NewEventPanel.Months;
import controllers.EventController;
import controllers.OrgController;
import controllers.UserController;
import models.CurrentObjects;

public class NewOrganizationPanel extends JPanel{
	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	DisplayPanel orgPanel;
	JPanel buttonsPanel;
	JLabel orgName = new JLabel("Organization Name: ");
	JLabel orgDescription = new JLabel("Description: ");
	JLabel meetingTimes = new JLabel("Meeting Times: ");
	JLabel contact = new JLabel("Contact: ");
	//JLabel memberNo;
	JLabel dues = new JLabel("Dues: ");
	JLabel audience = new JLabel("     Audience: ");
	
	JTextField orgNameField = new JTextField();
	JTextArea orgDescriptionArea = new JTextArea();
	JTextField meetingTimesField = new JTextField();
	JTextField contactField = new JTextField();
	JTextField duesField = new JTextField();
	String[] audienceArray = {"Undergrad", "Grad", "Both"};
	JComboBox<String> audienceCombo = new JComboBox<String>(audienceArray);
	
	JButton createOrgButton = new JButton("Create");
	JButton cancelButton = new JButton("Cancel");
	
	
	public NewOrganizationPanel( DisplayPanel displayPanel, DisplayPanel userPanel, DisplayPanel orgPanel)  {
		orgName.setFont(new Font("Garamond",Font.BOLD,14));
		orgDescription.setFont(new Font("Garamond",Font.BOLD,14));
		meetingTimes.setFont(new Font("Garamond",Font.BOLD,14));
		contact.setFont(new Font("Garamond",Font.BOLD,14));
		dues.setFont(new Font("Garamond",Font.BOLD,14));
		audience.setFont(new Font("Garamond",Font.BOLD,14));
		orgNameField.setFont(new Font("Garamond",Font.BOLD,14));
		orgDescriptionArea.setFont(new Font("Garamond",Font.BOLD,14));
		meetingTimesField.setFont(new Font("Garamond",Font.BOLD,14));
		contactField.setFont(new Font("Garamond",Font.BOLD,14));
		duesField.setFont(new Font("Garamond",Font.BOLD,14));
		audienceCombo.setFont(new Font("Garamond",Font.BOLD,14));
		createOrgButton.setFont(new Font("Garamond",Font.BOLD,14));
		cancelButton.setFont(new Font("Garamond",Font.BOLD,14));
		this.displayPanel = displayPanel;
		this.userPanel = userPanel;
		this.orgPanel = orgPanel;
		setLayout(new BorderLayout());
		setBackground(Color.white);
		add(getNewOrganizationPanel(), BorderLayout.CENTER);
		createOrgButton.setPreferredSize(new Dimension(150, 30));
	}
	
	JPanel getButtonsPanel() {
		buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getHeight()));
		
		createOrgButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String result = OrgController.newOrganization(orgNameField.getText(),  orgDescriptionArea.getText(), meetingTimesField.getText(), contactField.getText(), "1", duesField.getText(), (String)audienceCombo.getSelectedItem());
				if (result.equals("Success!")) {
					JOptionPane.showMessageDialog(buttonsPanel,"Organization Created!");
					CurrentObjects.setOrg(OrgController.getOrgWithName(orgNameField.getText()));
					UserController.addPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID(), "ADMIN");
					displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
					orgPanel.swap(new CurrentOrgPanel(displayPanel, userPanel, orgPanel));
				}
				else {
					JOptionPane.showMessageDialog(buttonsPanel, result, "Organization Error", JOptionPane.ERROR_MESSAGE);
				}
					
			}
			
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new WelcomePanel());
			}
		});
		
		buttonsPanel.add(createOrgButton);
		buttonsPanel.add(cancelButton);
		return buttonsPanel;
		
	}
	JPanel getNewOrganizationPanel() {
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.white);
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridBagLayout());
		innerPanel.setBackground(Color.white);
		GridBagConstraints c = new GridBagConstraints();
		
		
		orgDescriptionArea.setFont(new Font("Garamond",Font.BOLD,14));
		orgDescriptionArea.setEditable(true);	
		orgDescriptionArea.setLineWrap(true);
		orgDescriptionArea.setWrapStyleWord(true);
		JScrollPane descriptionScroll = new JScrollPane(orgDescriptionArea);
		
		
		JPanel namePanel = new JPanel();
		namePanel.setLayout(new FlowLayout());
		namePanel.setBackground(Color.white);
		orgNameField.setPreferredSize(new Dimension(300, 30));
		namePanel.add(orgName);
		namePanel.add(orgNameField);
		namePanel.add(audience);
		namePanel.add(audienceCombo);
		
		JPanel descriptionPanel = new JPanel();
		descriptionPanel.setBackground(Color.white);
		descriptionPanel.add(orgDescription);
		descriptionPanel.add(descriptionScroll);
		descriptionScroll.setPreferredSize(new Dimension(600, 100));
		
		JPanel miscPanel = new JPanel();
		miscPanel.setBackground(Color.white);
		miscPanel.setLayout(new FlowLayout());
		miscPanel.add(meetingTimes);
		meetingTimesField.setPreferredSize(new Dimension(150, 30));
		miscPanel.add(meetingTimesField);
		miscPanel.add(contact);
		contactField.setPreferredSize(new Dimension(150, 30));
		miscPanel.add(contactField);
		miscPanel.add(dues);
		duesField.setPreferredSize(new Dimension(150, 30));
		miscPanel.add(duesField);
		
		JPanel boxPanel = new JPanel();
		boxPanel.setBackground(Color.white);
		boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
		JLabel panelTitle = new JLabel("Create a New Organization");
		panelTitle.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel spacing = new JLabel(" ");
		boxPanel.add(panelTitle);
		boxPanel.add(spacing);
		boxPanel.add(namePanel);
		boxPanel.add(descriptionPanel);
		boxPanel.add(miscPanel);
		boxPanel.add(getButtonsPanel());
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_START;
		c.weighty = 1.0;
		innerPanel.add(boxPanel, c);
		
		mainPanel.add(innerPanel);
		
		return mainPanel;
		
	}
}
