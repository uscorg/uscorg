package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.ExecutorService;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import threads.CommentsThread;
import models.Announcement;
import models.Comment;
import models.CurrentObjects;
import controllers.AnnouncementController;
import controllers.CommentController;
import controllers.OrgController;
import controllers.UserController;
import database.AnnouncementDB;
import database.EventDB;
import database.UserDB;

public class AnnouncementPanel extends JPanel {
	
	JButton deleteAnnouncementButton;
	JButton backButton;
	JButton addCommentButton;
	JTextArea[] announcementPane;
	JScrollPane[] announcementScrollPane;
	JPanel buttonsPanel;

	private Announcement announcement;
	private CommentsThread commentsThread;
	private JPanel commentMainPanel;
	private JScrollPane commentScrollPane;
	private DisplayPanel displayPanel;
	
	public AnnouncementPanel(int orgID, String title, DisplayPanel displayPanel) {
		this.announcement = findAnnouncement(orgID, title);
		this.displayPanel = displayPanel;

		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		if (announcement == null) {
			add(new JLabel("This announcement does not exist. Good job, you broke the program."));
		} else {
			add(getButtonsPanel());
			add(getAnnouncementPanel());
			add(getCommentPanel());
			commentsThread = new CommentsThread(announcement.getAnnouncementID(), commentScrollPane, commentMainPanel, displayPanel);
			CurrentObjects.getExecutor().submit(commentsThread);
		}
	}
	
	JPanel getButtonsPanel() {
	    buttonsPanel = new JPanel(new FlowLayout());
		backButton = new JButton("Back");
		backButton.setFont(new Font("Garamond",Font.BOLD,14));
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				displayPanel.swap(new OrganizationPanel(OrgController.getOrgWithID(announcement.getOrg()).getOrgID(), displayPanel));
			}
		});
		buttonsPanel.add(backButton);

		//checking user access here
		if (CurrentObjects.getUser().isLoggedIn()) {
			if (UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN") 
					|| UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("MEMBER")) {
				//admin + member are able to comment
				addCommentButton = new JButton("Add Comment");
				addCommentButton.setFont(new Font("Garamond",Font.BOLD,14));
			    addCommentButton.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						displayPanel.swap(new NewCommentPanel(announcement, displayPanel));
					}
				});
				buttonsPanel.add(addCommentButton);
			}
			if (UserDB.getPermission(CurrentObjects.getUser().getUserID(), CurrentObjects.getOrg().getOrgID()).equals("ADMIN")) {
				//only admin should be able to delete
				deleteAnnouncementButton = new JButton ("Delete Announcement");
				deleteAnnouncementButton.setFont(new Font("Garamond",Font.BOLD,14));
				deleteAnnouncementButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(AnnouncementDB.deleteAnnouncement(announcement.getAnnouncementID())) {
							JOptionPane.showMessageDialog(buttonsPanel,"Announcement Deleted!");
							displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
						}
						else {
							JOptionPane.showMessageDialog(buttonsPanel,"Error deleting announcement!\nPlease try again!", "Announcement Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				});
				buttonsPanel.add(deleteAnnouncementButton);
			}
		}
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getPreferredSize().height));
		return buttonsPanel;
	}
	
	JPanel getAnnouncementPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		JLabel announcementTitle = new JLabel();
		JTextPane announcementText = new JTextPane();
		JLabel dateLabel = new JLabel();
		JScrollPane textScrollPane = new JScrollPane(announcementText);
		
		announcementTitle.setText(announcement.getTitle());
		announcementTitle.setFont(new Font("Garamond", Font.BOLD, 32));	

		announcementText.setText(announcement.getText());
		announcementText.setFont(new Font("Garamond", Font.ITALIC, 18));
		announcementText.setBorder(BorderFactory.createEmptyBorder(4, 0, 5, 0));
		announcementText.setEditable(false);
		//announcementText.setLineWrap(true);
		//announcementText.setWrapStyleWord(true);
		announcementText.setFocusable(false);
		announcementText.setOpaque(false);
		
		//textScrollPane.setPreferredSize(commentScrollPane.getPreferredSize());

		dateLabel.setText("Date posted: " + new Date(announcement.getDate().getTime()));
		dateLabel.setFont(new Font("Garamond", Font.PLAIN, 14));
		
		mainPanel.add(announcementTitle);
		mainPanel.add(textScrollPane);
		mainPanel.add(dateLabel);
		mainPanel.setPreferredSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), mainPanel.getPreferredSize().height));
		return mainPanel;
	}
	
	JScrollPane getCommentPanel(){
		commentMainPanel = new JPanel();
		commentMainPanel.setBorder(BorderFactory.createTitledBorder("Comments"));
		commentMainPanel.setLayout(new BoxLayout(commentMainPanel, BoxLayout.Y_AXIS));
		commentMainPanel.add(new JLabel("Loading..."));

		commentScrollPane = new JScrollPane(commentMainPanel);
		commentScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		return commentScrollPane;
	}
	
	private Announcement findAnnouncement(int orgID, String title) {
		Announcement result = null;
		Vector<Announcement> orgAnnouncements = AnnouncementController.getAnnouncement(orgID);
		for (int i = 0; i < orgAnnouncements.size(); i++) {
			if (orgAnnouncements.get(i).getTitle().equals(title)) {
				result = orgAnnouncements.get(i);
				return result;
			}
		}
		return result;
	}
	
}