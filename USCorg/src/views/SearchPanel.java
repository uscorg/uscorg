package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class SearchPanel extends JPanel {
	
	public String searchText;
	public JButton searchBut;
	public JTextField searchTextfield;

	public SearchPanel(final DisplayPanel displayPanel) {		
		searchTextfield = new JTextField();
		JLabel titleLab = new JLabel("  Search Organizations:");
		titleLab.setFont(new Font("Garamond", Font.BOLD, 14));
		titleLab.setForeground(Color.DARK_GRAY);
		JLabel blankLab1 = new JLabel("");
		JLabel blankLab2 = new JLabel("");
		JLabel searchLab = new JLabel("  Search: ");
		searchLab.setFont(new Font("Garamond", Font.BOLD, 14));
		searchLab.setForeground(Color.DARK_GRAY);
		searchBut = new JButton("Search");
		searchBut.setFont(new Font("Garamond", Font.BOLD, 14));
		searchTextfield.setFont(new Font("Garamond", Font.BOLD, 14));
		searchTextfield.addActionListener(new ActionListener() {
			/*
			 * ActionListener for when Enter is pressed in the search field.
			 * Executes the same action as the searchBut's ActionListener.
			 */
			public void actionPerformed(ActionEvent e) {
				//technically should check if the button even has an action listener,
				//but we know it will have it, because we are adding it right below
				searchBut.getActionListeners()[0].actionPerformed(e);
			}
			
		});
		searchBut.setForeground(Color.DARK_GRAY);
		searchBut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 if (searchTextfield.getText().equals("")) return;
				JScrollPane searchScroll = new JScrollPane(new SearchResultsPanel(searchTextfield.getText(), displayPanel));
				searchScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
				displayPanel.swap(searchScroll);
			}
		});
		setLayout(new GridLayout(3,2));
		add(titleLab);
		add(blankLab1);
		add(searchLab);
		add(searchTextfield);
		add(blankLab2);
		add(searchBut);
		setBackground(Color.LIGHT_GRAY);
	}

}
