package views;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import models.CurrentObjects;

public class MainFrame extends JFrame{	
	
	private DisplayPanel displayPanel = new DisplayPanel();
	private JPanel eastPanel = new JPanel();
	private JPanel westPanel = new JPanel();
	private JPanel centerPanel = new JPanel();
	private DisplayPanel userPanel = new DisplayPanel();
	private DisplayPanel orgPanel = new DisplayPanel();
	private static Dimension centerPanelDimension;
	
	public MainFrame()  {
		super("USC.org");
		super.setLayout(new BorderLayout());
		userPanel.setBackground(Color.DARK_GRAY);
		userPanel.setMaximumSize(new Dimension(300, userPanel.getPreferredSize().width));
		eastPanel.setLayout(new BoxLayout(eastPanel, BoxLayout.Y_AXIS));
		eastPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		westPanel.setLayout(new BoxLayout(westPanel, BoxLayout.Y_AXIS));
		westPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		add(eastPanel, BorderLayout.EAST);
		add(westPanel, BorderLayout.WEST);
		add(centerPanel, BorderLayout.CENTER);
				
		orgPanel.setMaximumSize(new Dimension(500, orgPanel.getPreferredSize().width));
		LogoPanel logoPanel = new LogoPanel();

		SearchPanel searchPanel = new SearchPanel(displayPanel);
		searchPanel.setMaximumSize(new Dimension(400, searchPanel.getPreferredSize().width));
		searchPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		CategoryPanel categoryPanel = new CategoryPanel(displayPanel);
		categoryPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		NotificationPanel notificationPanel = new NotificationPanel(displayPanel);
		notificationPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		//CurrentOrgPanel currentorgPanel = new CurrentOrgPanel(displayPanel, userPanel);
		//currentorgPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		LoginPanel loginPanel = new LoginPanel(displayPanel, userPanel, orgPanel);
		
		//userPanel.setLayout(new BorderLayout());
		userPanel.add(loginPanel);

		
		JPanel blankPanel = new JPanel();
		blankPanel.setBackground(Color.WHITE);
		
		//calculating the display panel width, to be used by the entire program later
		CurrentObjects.setDisplayPanelWidth((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth()-westPanel.getWidth()-eastPanel.getWidth());

		displayPanel.setBackground(Color.WHITE);
		JScrollPane categoryScroll = new JScrollPane(categoryPanel);
		categoryScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		categoryScroll.setSize(400,500);
		displayPanel.swap(new WelcomePanel());
		orgPanel.swap(new CurrentOrgPanel(displayPanel, userPanel, orgPanel));
		JScrollPane scroll = new JScrollPane(displayPanel);
		scroll.setMinimumSize(centerPanel.getPreferredSize());
		logoPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), logoPanel.backimage.getHeight()));
		centerPanel.add(logoPanel);
		centerPanel.add(scroll);
		centerPanel.getSize(centerPanelDimension);
		//centerPanel.add(blankPanel);
		
		westPanel.add(userPanel);
		westPanel.add(searchPanel);
		westPanel.add(categoryScroll);
		//westPanel.add(new AutocompleteJComboBox(list));
		westPanel.add(new JPanel());
		westPanel.add(new JPanel());
		westPanel.add(new JPanel());

		eastPanel.add(orgPanel);
		eastPanel.add(notificationPanel);
		eastPanel.add(new JPanel());

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setVisible(true);
		setResizable(false);
		validate();
	}
	
	public static Dimension getCenterPanelDimension() {
		return centerPanelDimension;
	}

}