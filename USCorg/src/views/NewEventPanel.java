package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import models.CurrentObjects;
import controllers.AnnouncementController;
import controllers.EventController;

public class NewEventPanel extends JPanel{
	JButton createButton = new JButton ("Create Event");
	JButton cancelButton = new JButton ("Cancel");
	JPanel buttonsPanel;

	JTextField titleTextField = new JTextField();
	JTextArea textTextField = new JTextArea();
	public enum Months {
		JANUARY(1), FEBRUARY(2), MARCH(3), APRIL(4), MAY(5), JUNE(6), JULY(7), AUGUST(8), SEPTEMBER(9), NOVEMBER(10), DECEMBER(11);
		private int value;
		private Months(int value) {
			this.value = value;
		}
		public int getValue() {
			return this.value;
		}
	}
	public enum Times {
		ONE(1, "1:00AM"), TWO(2, "2:00AM"), THREE(3, "3:00AM"), FOUR(4, "4:00AM"), FIVE(5, "5:00AM"), SIX(6, "6:00AM"), SEVEN(7, "7:00AM"),EIGHT(8, "8:00AM"), 
		NINE(9, "9:00AM"), TEN(10, "10:00AM"), ELEVEN(11, "11:00AM"), TWELVE(12, "12:00PM"),THIRTEEN(13, "1:00PM"), FOURTEEN(14, "2:00PM"), FIFTEEN(15, "3:00PM"), SIXTEEN(16, "4:00PM"), SEVENTEEN(17, "5:00PM"), 
		EIGHTEEN(18, "6:00PM"), NINETEEN(19, "7:00PM"), TWENTY(20, "8:00PM"), TWENTYONE(21, "9:00PM"), TWENTYTWO(22, "10:00PM"), TWENTYTHREE(23, "11:00PM"), TWENTYFOUR(24, "12:00AM");
		private int value;
		private String text;
		
		private Times(int value, String text) {
			this.value = value;
			this.text = text;
		}
		
		public int getValue() {
			return this.value;
		}
		public String getText() {
			return this.text;
		}
	}
	Integer[] timeArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	
	Integer [] dateArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 
			12, 13, 14, 15, 16, 17, 18, 19,20, 21, 22, 23, 24, 25, 26, 27, 28, 29,30, 31};
	Integer [] yearArray = {2015, 2016, 2017, 2018, 2019};
	
	String [] ampmArray = {"AM", "PM"};
	
	JComboBox monthsCombo = new JComboBox(Months.values());
	JComboBox<Integer> timeCombo = new JComboBox<Integer>(timeArray);
	JComboBox<String> ampmCombo = new JComboBox<String>(ampmArray);
	JComboBox <Integer> dateCombo = new JComboBox<Integer>(dateArray);
	JComboBox <Integer> yearCombo = new JComboBox<Integer>(yearArray);

	JLabel monthLabel = new JLabel("Month: ");
	JLabel dateLabel = new JLabel ("Date: ");
	JLabel yearLabel = new JLabel ("Year: ");
	JLabel timeLabel = new JLabel ("Time: ");
	JLabel panelLabel = new JLabel("New Event");
	
	
	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	
	public NewEventPanel( DisplayPanel displayPanel)  {
		createButton.setFont(new Font("Garamond",Font.BOLD,14));
		cancelButton.setFont(new Font("Garamond",Font.BOLD,14));
		monthLabel.setFont(new Font("Garamond",Font.BOLD,14));
		dateLabel.setFont(new Font("Garamond",Font.BOLD,14));
		yearLabel.setFont(new Font("Garamond",Font.BOLD,14));
		timeLabel.setFont(new Font("Garamond",Font.BOLD,14));
		panelLabel.setFont(new Font("Garamond",Font.BOLD,14));
		yearCombo.setFont(new Font("Garamond",Font.BOLD,14));
		dateCombo.setFont(new Font("Garamond",Font.BOLD,14));
		ampmCombo.setFont(new Font("Garamond",Font.BOLD,14));
		timeCombo.setFont(new Font("Garamond",Font.BOLD,14));
		monthsCombo.setFont(new Font("Garamond",Font.BOLD,14));
		titleTextField.setFont(new Font("Garamond",Font.BOLD,14));
		textTextField.setFont(new Font("Garamond",Font.BOLD,14));
		this.displayPanel = displayPanel;
		setLayout(new BorderLayout());
		setBackground(Color.white);
		//setPreferredSize(new Dimension(800, 600));
		
		//panelLabel.setPreferredSize(new Dimension(200, 30));
		//add(panelLabel, BorderLayout.NORTH);
		
		add(getNewEventPanel(), BorderLayout.CENTER);
		createButton.setPreferredSize(new Dimension(150, 30));
		//add(createButton, BorderLayout.SOUTH);
	}
	JPanel getButtonsPanel() {
		buttonsPanel = new JPanel(new FlowLayout());
		buttonsPanel.setBackground(Color.white);
		buttonsPanel.setMaximumSize(new Dimension(CurrentObjects.getDisplayPanelWidth(), buttonsPanel.getHeight()));
		
		createButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int time = (Integer)timeCombo.getSelectedItem();
				if (((String)ampmCombo.getSelectedItem()).equals("PM")) {
					time = time + 12;
				}
				String result = EventController.newEvent(CurrentObjects.getOrg().getOrgID(),  (int)yearCombo.getSelectedItem(), ((Months)monthsCombo.getSelectedItem()).getValue(), (int)dateCombo.getSelectedItem(), textTextField.getText(), titleTextField.getText(), time);
				if (result.equals("Success!")) {
					JOptionPane.showMessageDialog(mainPanel,"Event Created!");
				}
				else if (result.equals("Event descriptions must be less than 160 characters!")){
					JOptionPane.showMessageDialog(mainPanel,result, "Event Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					JOptionPane.showMessageDialog(mainPanel,"Error creating event!\nPlease try again!", "Event Error", JOptionPane.ERROR_MESSAGE);
				}
					
			}
			
		});
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayPanel.swap(new OrganizationPanel(CurrentObjects.getOrg().getOrgID(), displayPanel));
			}
		});
		
		buttonsPanel.add(createButton);
		buttonsPanel.add(cancelButton);
		return buttonsPanel;		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	JPanel getNewEventPanel() {
		
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		mainPanel.setBackground(Color.white);
		
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout(new GridBagLayout());
		innerPanel.setBackground(Color.white);
		
		GridBagConstraints c = new GridBagConstraints();
		
		textTextField.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam tortor massa, tincidunt a nisi finibus, ultrices finibus urna. Proin vitae vehicula dolor.");
		textTextField.setEditable(true);	
		textTextField.setLineWrap(true);
		textTextField.setWrapStyleWord(true);
		JScrollPane textScroll = new JScrollPane(textTextField);
		
		JLabel titleLabel = new JLabel("Event Name: ");
		titleLabel.setFont(new Font("Garamond",Font.BOLD,14));
		JLabel textLabel = new JLabel("Event Description: ");
		textLabel.setFont(new Font("Garamond",Font.BOLD,14));
	
		
		JPanel timePanel = new JPanel();
		timePanel.setLayout(new BoxLayout(timePanel, BoxLayout.LINE_AXIS));
		timePanel.setBackground(Color.white);
		JLabel spacing = new JLabel("      ");
		JLabel spacing2 = new JLabel("      ");
		JLabel spacing3 = new JLabel("      ");
		JLabel spacing4 = new JLabel("      ");
		JLabel spacing5 = new JLabel("      ");
		timePanel.add(spacing);
		timePanel.add(monthLabel);
		timePanel.add(monthsCombo);
		timePanel.add(spacing3);
		timePanel.add(dateLabel);
		timePanel.add(dateCombo);
		timePanel.add(spacing4);
		timePanel.add(yearLabel);
		timePanel.add(yearCombo);
		timePanel.add(spacing2);
		timePanel.add(timeLabel);
		timePanel.add(timeCombo);
		timePanel.add(ampmCombo);
		timePanel.add(spacing5);
		
		
		
		JPanel titlePanel = new JPanel();
		titlePanel.setBackground(Color.white);
		titleTextField.setPreferredSize(new Dimension(600, 30));
		titlePanel.add(titleLabel);
		titlePanel.add(titleTextField);

		
		JPanel textPanel = new JPanel();
		textPanel.setBackground(Color.white);
		//textTextField.setPreferredSize(new Dimension(200, 30));
		textPanel.add(textLabel);
		textPanel.add(textScroll);
		textScroll.setPreferredSize(new Dimension(600, 60));
		
		JPanel boxPanel = new JPanel();
		boxPanel.setBackground(Color.white);
		boxPanel.setLayout(new BoxLayout(boxPanel, BoxLayout.PAGE_AXIS));
		
		JLabel blankLabel = new JLabel(" ");
		JLabel blankLabel2 = new JLabel(" ");
		boxPanel.add(panelLabel);
		boxPanel.add(blankLabel2);
		boxPanel.add(titlePanel);
		boxPanel.add(timePanel);
		boxPanel.add(blankLabel);
		boxPanel.add(textPanel);
		boxPanel.add(getButtonsPanel());
		
		c.gridx = 1;
		c.gridy = 1;
		c.anchor = GridBagConstraints.PAGE_START;
		c.weighty = 1.0;
		innerPanel.add(boxPanel, c);

		mainPanel.add(innerPanel);

		
		

		return mainPanel;
	}
}
