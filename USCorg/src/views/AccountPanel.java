package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controllers.UserController;

/* Registration panel that will go in place of the central
 * display panel in the GUI. Shows up when a user clicks
 * the "Click to Register" button.
 */

//**Priyam: need to add last two textfields to the GUI


public class AccountPanel extends JPanel {
	JPanel mainPanel = new JPanel();

	DisplayPanel displayPanel;
	DisplayPanel userPanel;
	
	JButton editButton = new JButton ("Edit Account");
	JTextField firstNameTextField = new JTextField(models.CurrentObjects.getUser().getFirstName());
	JTextField lastNameTextField = new JTextField(models.CurrentObjects.getUser().getLastName());
	JTextField emailTextField = new JTextField(models.CurrentObjects.getUser().getUscName() + "@usc.edu");
	JTextField idTextField = new JTextField(models.CurrentObjects.getUser().getStudentID());
	JPasswordField passwordTextField = new JPasswordField();
	JPasswordField confirmPasswordTextField = new JPasswordField();
	JTextField majorTextField = new JTextField(models.CurrentObjects.getUser().getMajor());
	JComboBox yearBox = new JComboBox(new String[]{"2015","2016","2017","2018","2019"});


	
	public AccountPanel(DisplayPanel displayPanel, DisplayPanel userPanel)  {
		editButton.setFont(new Font("Garamond", Font.BOLD, 14));
		firstNameTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		lastNameTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		emailTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		idTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		passwordTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		confirmPasswordTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		majorTextField.setFont(new Font("Garamond", Font.BOLD, 14));
		yearBox.setFont(new Font("Garamond", Font.BOLD, 14));
		
		this.displayPanel = displayPanel;
		this.userPanel = userPanel;
		String yearGrad = Integer.toString(models.CurrentObjects.getUser().getYearOf());
		for(int i = 0;i < yearBox.getItemCount();i++)
		{
			if(yearGrad.equals(yearBox.getItemAt(i)))
			{
				yearBox.setSelectedIndex(i);
			}
		}
		
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Color.white);
		JLabel titleLabel = new JLabel("    Edit Account                             \n");
		titleLabel.setForeground(Color.BLUE);
		titleLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel blankLabel = new JLabel("Personal Information    ");
		blankLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		blankLabel.setForeground(Color.RED);
		add(titleLabel);
		add(blankLabel);
		add(getAccountPanel());
				
		
	}
	
	JPanel getAccountPanel() {
		mainPanel.setLayout(new GridLayout(16,2));
		mainPanel.setBackground(Color.white);
		
		JLabel firstnameLabel = new JLabel("    First Name: ");
		firstnameLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel lastnameLabel = new JLabel("    Last Name: ");
		lastnameLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel emailLabel = new JLabel("    USC Email: ");
		emailLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel idLabel = new JLabel("    USC ID #: ");
		idLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel passwordLabel = new JLabel("    New Password: ");
		passwordLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel confirmPasswordLabel = new JLabel("    Confirm Password: ");
		confirmPasswordLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel majorLabel = new JLabel("    Major(s): ");
		majorLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		JLabel yearLabel = new JLabel("     Year of Graduation: ");
		yearLabel.setFont(new Font("Garamond", Font.BOLD, 14));
		
		editButton.addActionListener(new ActionListener() {
			 
            @Override
			public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
            	String studentID = idTextField.getText();
            	String fname = firstNameTextField.getText();
            	String lname = lastNameTextField.getText();
            	String pw = new String(passwordTextField.getPassword());
            	String confirmPW = new String (confirmPasswordTextField.getPassword());
            	String major = majorTextField.getText();
            	String uscName = emailTextField.getText();
            	int yearOf;
            	try {
            		yearOf =  Integer.parseInt((String) yearBox.getSelectedItem());
            	} catch (NumberFormatException nfe) {
            		yearOf = models.CurrentObjects.getUser().getYearOf();	            	}

            	String result = UserController.changeUserInfo(studentID, fname, lname, pw, confirmPW, major, yearOf, uscName);
            	if (result.equals("SUCCESS")) {
            		JOptionPane.showMessageDialog(mainPanel, result, "Update Successful!", JOptionPane.WARNING_MESSAGE);
            	} else {
            		JOptionPane.showMessageDialog(mainPanel, result, "Update Unsuccessful", JOptionPane.WARNING_MESSAGE);
            	}
            	
            }
        }); 
		
		mainPanel.add(firstnameLabel);
		mainPanel.add(firstNameTextField);
		mainPanel.add(lastnameLabel);
		mainPanel.add(lastNameTextField);
		mainPanel.add(emailLabel);
		mainPanel.add(emailTextField);
		mainPanel.add(idLabel);
		mainPanel.add(idTextField);
		mainPanel.add(majorLabel);
		mainPanel.add(majorTextField);
		mainPanel.add(yearLabel);
		mainPanel.add(yearBox);
		mainPanel.add(passwordLabel);
		mainPanel.add(passwordTextField);
		mainPanel.add(confirmPasswordLabel);
		mainPanel.add(confirmPasswordTextField);
		mainPanel.add(new JLabel());
		mainPanel.add(editButton);
				
		return mainPanel;
	}
}