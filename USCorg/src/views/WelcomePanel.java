package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class WelcomePanel extends JPanel {
	//TODO UPDATE THIS PLEASE
	/*
	 * This panel will be displayed in displayPanel whenever the application is started,
	 * or whenever a user logs out. Essentially, it is the welcome screen.
	 */
	public WelcomePanel() {
		setLayout(new BorderLayout());
		Color col = new Color(255,204,0);
		this.setBackground(col);
		this.setBorder(BorderFactory.createLineBorder(col));
		JLabel titleLabel = new JLabel("Welcome to USC.Org!");
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		titleLabel.setFont(new Font("Garamond", Font.BOLD, 40));
		
		JLabel endingLabel = new JLabel("This world is really your oyster!");
		endingLabel.setHorizontalAlignment(JLabel.CENTER);
		endingLabel.setFont(new Font("Garamond", Font.BOLD, 50));
	
		
		JTextPane infoLabel = new JTextPane();
		infoLabel.setText("\n This is USC.Org, the convenient way to communicate with your on-campus organizations.\n \n"
				+ "In order to get started, login in the top left corner, or register to make an account.\n \n"
				+ "Right now, you can also search and browse already existing organizations. \n \n"
				+ "However, once logged in, you can:\n"
				+ "       Be part of as many organizations as you would like!\n"
				+ "       Check your organization's announcements and events!\n"
				+ "       Comment on your organizations! \n \n \n \n");
		infoLabel.setEditable(false);
		infoLabel.setFont(new Font("Garamond", Font.BOLD, 20));
		StyledDocument doc = infoLabel.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);

		infoLabel.setFocusable(false);
		infoLabel.setOpaque(false);		
		add(titleLabel, BorderLayout.NORTH);
		add(infoLabel, BorderLayout.CENTER);
		add(endingLabel, BorderLayout.SOUTH);
	}
}
