package views;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class DisplayPanel extends JPanel {
	/*
	 * This class is used for all panels with interchangeable content.
	 * For example, the central displayPanel that constantly has its
	 * content replaced with user input.
	 * 
	 * The class is mainly for convenience, as the function swap() is
	 * used throughout the GUI code.
	 */
	
	private boolean threadChecker = false;
	private boolean wasForced = false;
	
	public DisplayPanel() {
		setLayout(new BorderLayout());
	}
	
	public void swap (JPanel newPanel) {
		if (!wasForced) {
			threadChecker = !threadChecker;
		} else {
			wasForced = false;
		}
		removeAll();
		add(newPanel);
		validate();
		repaint();
	}
	public void swap (JScrollPane newPanel) {
		if (!wasForced) {
			threadChecker = !threadChecker;
		} else {
			wasForced = false;
		}
		removeAll();
		add(newPanel);
		validate();
		repaint();
	}
	public boolean getBool() {
		return threadChecker;
	}
	public void switchBool() {
		threadChecker = !threadChecker;
		wasForced = true;
	}
}
