package views;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LogoPanel extends JPanel {
	
	BufferedImage backimage = null;
		//i'll customize this to look nice once we polish everything else

	public LogoPanel() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		try {
			final BufferedImage usclogo = ImageIO.read(new File("USC.jpg"));
			this.backimage = usclogo;
			JLabel imageLabel = new JLabel();
			imageLabel.setIcon(new ImageIcon(backimage));
			add(imageLabel);
			setSize(usclogo.getWidth(),usclogo.getHeight());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Color cardinal = new Color(153, 27, 30);
		setBackground(cardinal);
	}

}
